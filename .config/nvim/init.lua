-- quick exit for vscode's neovim extension
if (vim.g.vscode)
  then
    return
end

require('ac130kz')
