require('lualine').setup({
    options = {
        theme = 'codedark',
        -- theme = 'tokyonight',
        -- theme = 'kanagawa-wave',
        component_separators = { left = '|', right = '|' },
        section_separators = { left = '', right = '' },
    },
    sections = {
        lualine_x = { 'encoding', 'filetype' }
    }
})
