local telescope = require("telescope")
telescope.setup({
    defaults = {
        vimgrep_arguments = {
            'rg',
            '--color=never',
            '--no-heading',
            '--with-filename',
            '--line-number',
            '--column',
            '--smart-case'
        },
        prompt_prefix     = ' 🔍 ',
    },
    extensions = {
        'flutter',
        fzf = {
            fuzzy = true,                   -- false will only do exact matching
            override_generic_sorter = true, -- override the generic sorter
            override_file_sorter = true,    -- override the file sorter
            case_mode = "smart_case",       -- or "ignore_case" or "respect_case"
            -- the default case_mode is "smart_case"
        },
        file_browser = {
            theme = "ivy"
        }
        --file_browser = {
        --   theme = "ivy",
        --    mappings = {
        --      ["i"] = {
        --        -- your custom insert mode mappings
        --      },
        --      ["n"] = {
        --        -- your custom normal mode mappings
        --      },
        --},
    }
})
telescope.load_extension('fzf')
telescope.load_extension('harpoon')
telescope.load_extension('file_browser')

-- keymaps
local builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>ff", builtin.find_files, {})
vim.keymap.set("n", "<leader>fg", builtin.live_grep, {})
vim.keymap.set("n", "<leader>fb", builtin.buffers, {})
vim.keymap.set("n", "<leader>fh", builtin.help_tags, {})

--------------- Telescope
vim.api.nvim_create_autocmd({ "FileType" }, {
    pattern = { "TelescopePrompt" },
    callback = function()
        require('cmp').setup.buffer { enabled = false }
    end
})
