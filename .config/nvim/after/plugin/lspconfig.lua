local lspconfig = require("lspconfig")

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
    group = vim.api.nvim_create_augroup('UserLspConfig', {}),
    callback = function(ev)
        local opts = { silent = true, buffer = ev.buf }

        -- JUST A REFERENCE
        -- See `:help vim.lsp.*` for documentation on any of the below functions
        --  vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
        --  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
        --  vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, opts)
        --  vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, opts)
        --  vim.keymap.set('n', '<space>wl', print(vim.inspect(vim.lsp.buf.list_workspace_folders())), opts)
        --  vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, opts)
        --  vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, opts)
        --  vim.keymap.set('n', '<space>e', vim.lsp.diagnostic.show_line_diagnostics, opts)
        --  vim.keymap.set('n', '[d', vim.lsp.diagnostic.goto_prev, opts)
        --  vim.keymap.set('n', ']d', vim.lsp.diagnostic.goto_next, opts)
        --  vim.keymap.set('n', '<space>q', vim.lsp.diagnostic.set_loclist, opts)
        --  vim.keymap.set('n', '<space>f', vim.lsp.buf.formatting, opts)

        -- lsp keymaps definitions references
        vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
        vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
        vim.keymap.set("n", "gr", vim.lsp.buf.references, opts)
        vim.keymap.set("n", "gy", vim.lsp.buf.type_definition, opts)
        vim.keymap.set("n", "gi", vim.lsp.buf.implementation, opts)
        vim.keymap.set('n', '<Leader>f', function()
            vim.lsp.buf.format { async = true }
        end, opts)

        -- TODO: signature_help()
        -- rename()
        -- hover()
        -- code_action()
        -- vim.lsp.diagnostic.goto_next
        -- vim.lsp.diagnostic.show_line_diagnostics(); vim.lsp.util.show_line_diagnostics()

        -- NOTE: this conflicts with null_ls style formatting, maybe do not use
        --
        -- autoformat on save
        -- FIXME: if client.server_capabilities.documentFormattingProvider then
        -- WORKS
        --vim.api.nvim_create_autocmd({ "BufWritePre" }, {
        --    pattern = { "<buffer>" },
        --    callback = function()
        --        vim.lsp.buf.format { async = true }
        --    end
        --})
        --end

        -- show function signature
        require("lsp_signature").on_attach({}, ev.buf)
    end,
})

-- INSTALLATION: sudo npm -g install pyright typescript-language-server
------------------------------ Global language config

-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
local servers = { 'rust_analyzer', 'pyright', 'ruff_lsp', 'tsserver', "lua_ls" }

-- The nvim-cmp almost supports LSP's capabilities so You should advertise it to LSP servers..
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)
for _, lsp in ipairs(servers) do
    lspconfig[lsp].setup {
        capabilities = capabilities,
        flags = { debounce_text_changes = 100 }
    }
end

----------------------------- Per language config overrides

-- python
-- TODO: port
--     "python.pythonPath": "${workspaceFolder}/.venv/bin/python",
--     "python.analysis.extraPaths": [
--        "${workspaceFolder}/.venv",
--    ],
--    "python.autoComplete.extraPaths": [
--        "${workspaceFolder}/.venv",
--    ],
-- TODO: https://github.com/microsoft/pyright/blob/main/docs/configuration.md#sample-pyprojecttoml-file
lspconfig.pyright.setup {
    cmd = { "pyright-langserver", "--stdio", "--poll" },
    -- TODO: do I need to override this stuff?? yeah... cause it overrides every time....
    -- flags = {debounce_text_changes = 100},
    -- on_attach = on_attach,
    -- capabilities = capabilities,

    --    settings = {
    --        python = {
    --        }
    --    }
}

-- Configure `ruff-lsp`.
-- See: https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#ruff_lsp
-- For the default config, along with instructions on how to customize the settings
lspconfig.ruff_lsp.setup {
    --on_attach = on_attach,
    filetypes = { 'python' },
    init_options = {
        settings = {
            -- Any extra CLI arguments for `ruff` go here.
            args = {},
        }
    }
}

lspconfig.rust_analyzer.setup {
    cmd = { "rustup", "run", "stable", "rust-analyzer" }
}

lspconfig.tsserver.setup {}

lspconfig.lua_ls.setup {
    settings = {
        Lua = {
            runtime = {
                -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
                version = 'LuaJIT',
            },
            diagnostics = {
                -- Get the language server to recognize the `vim` global
                globals = { 'vim' },
            },
            workspace = {
                -- Make the server aware of Neovim runtime files
                library = vim.api.nvim_get_runtime_file("", true),
            },
            -- Do not send telemetry data containing a randomized but unique identifier
            telemetry = {
                enable = false,
            },
            format = {
                enable = true,
                -- Put format options here
                -- NOTE: the value should be STRING!!
                defaultConfig = {
                    indent_style = "space",
                    indent_size = "2",
                }
            },
        },
    },
}
-- https://github.com/williamboman/mason.nvim
--https://github.com/folke/dot/tree/master/nvim/lua/util
--https://github.com/nvim-lua/kickstart.nvim/blob/master/init.lua
--https://www.youtube.com/watch?v=aqlxqpHs-aQ
--https://www.youtube.com/watch?v=Mccy6wuq3JE
