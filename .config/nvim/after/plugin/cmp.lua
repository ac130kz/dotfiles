local cmp = require('cmp')
local snippy = require('snippy')

-- TODO: discover native_menu, although it removes this
cmp.setup.cmdline(':', {
    sources = cmp.config.sources({
        { name = 'path' }
    }, {
        { name = 'cmdline' }
    })
})
cmp.setup.cmdline('/', {
    sources = { { name = 'buffer' }
    }
})
cmp.setup({
    enabled = true,
    snippet = {
        -- REQUIRED - you must specify a snippet engine
        expand = function(args)
            snippy.expand_snippet(args.body) -- For `snippy` users.
        end,
    },
    completion = {
        completeopt = "menu,menuone,preview,noinsert"
        -- show menu, show even for one, show preview, don't insert unless specified
    },
    formatting = {
        format = function(entry, vim_item)
            local kind_icons = {
                Text = "",
                Method = "",
                Function = "",
                Constructor = "",
                Field = "",
                Variable = "",
                Class = "ﴯ",
                Interface = "",
                Module = "",
                Property = "ﰠ",
                Unit = "",
                Value = "",
                Enum = "",
                Keyword = "",
                Snippet = "",
                Color = "",
                File = "",
                Reference = "",
                Folder = "",
                EnumMember = "",
                Constant = "",
                Struct = "",
                Event = "",
                Operator = "",
                TypeParameter = ""
            }
            -- This concatonates the icons with the name of the item kind
            vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind)

            -- Source
            vim_item.menu = ({
                buffer = "[Buffer]",
                nvim_lsp = "[LSP]",
                snippy = "[Snippy]",
                nvim_lua = "[Lua]",
                latex_symbols = "[LaTeX]",
                path = "[Path]",
            })[entry.source.name]
            return vim_item
        end
    },
    autocomplete = true,
    debug = false,
    min_length = 1,
    preselect = "enable",
    throttle_time = 100,
    source_timeout = 200,
    incomplete_delay = 400,
    allow_prefix_unmatch = false,
    mapping = {
        ['<CR>'] = cmp.mapping(function(fallback)
            if
                cmp.visible()
                and cmp.confirm({
                    behavior = cmp.ConfirmBehavior.Replace,
                    select = false,
                })
            then
                return
            else
                fallback()
            end
        end),
        ['<Down>'] = cmp.mapping(function(fallback)
            --elseif is_emmet_active() then
            --  return vim.fn['cmp#complete']()
            if cmp.visible() then
                cmp.select_next_item()
            elseif snippy.can_expand_or_advance() then
                snippy.expand_or_advance()
            elseif snippy.can_jump(1) then
                snippy.next()
            else
                fallback()
            end
        end, {
            'i',
            's',
        }),
        ['<Up>'] = cmp.mapping(function(fallback)
            --elseif is_emmet_active() then
            --  return vim.fn['cmp#complete']()
            --  https://github.com/shurizzle/neovimmizzle/blob/main/lua/config/plugins/cmp.lua#L295
            --  https://github.com/dcampos/nvim-snippy/blob/0b898935939cd71f27a818cfccac743ec473eeb3/lua/snippy/mapping.lua
            if cmp.visible() then
                cmp.select_prev_item()
            elseif snippy.can_expand_or_advance() then
                snippy.expand_or_advance()
            elseif snippy.can_jump(-1) then
                snippy.previous()
            else
                fallback()
            end
        end, {
            'i',
            's',
        }),
    },
    sources = {
        { name = 'nvim_lsp' },
        { name = 'buffer' },
        { name = 'path' },
        { name = 'snippy' },
        -- calc = false,
        -- nvim_lsp = true,
        -- nvim_lua = true,
        -- spell = false,
        -- tags = true,
        -- luasnip = true,
        -- orgmode = true,
        -- treesitter = false,
        -- snippets_nvim = false,
        -- vsnip = false,
        -- vim_dadbod_completion = true,
    }
})

-- TODO: port
--  require("nvim-autopairs.completion.cmp").setup({
--      map_complete = true -- it will auto insert `(` after select function or method item
--  })
