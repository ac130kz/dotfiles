require('nvim-treesitter.configs').setup({
    auto_install = true,
    ensure_installed = {
        "bash", "c", "cmake", "comment", "cpp", "css", "cuda",
        "dart", "html", "java", "javascript", "json", "latex",
        "lua", "python", "rust", "typescript", "make",
        "nix", "graphql", "fish", "markdown", "markdown_inline"
    },
    highlight = {
        enable = true,
        disable = {},
        additional_vim_regex_highlighting = false,
    },
    indent = {
        enable = true,
        disable = {},
    },
    context_commentstring = {
        enable = true,
        enable_autocmd = false,
    },
    -- TODO: broken 'nvim-treesitter/nvim-treesitter-refactor'
    --refactor = { highlight_definitions = { enable = true } },
    -- 'windwp/nvim-autopairs'
    autopairs = { enable = true }
})
