--------------------------------- Small guide
* splits: Ctrl + w -> v/s
* % - find mathing parenthesis
* / - search (? will search backwards), n - jumps to the next, N - jumps to the previous
* Ctrl+o - back, Ctrl+i - forward
* G - the end of the file, gg - the start of the file
* c - change, select something with visual mode or cw to change a word
* o - `open` a newline and INSERT, O - `open` a newline above and INSERT
* A - append to the line, a - append to the word, I - prepend to the line
* :%s/pattern/replacewith/g - replace (g this line, % all lines, adding c will introduce a prompt)
* :!COMMAND - execute the shell command below (non-scrollable, just use the terminal)
* :new - open a new window
* :terminal - open the terminal
* :r FILENAME - insert from a file or command!
* '<,'> - inside the visual selection
* gd - jump to the definition
* K - look up in the documentation
* :tabnew - new tab, gt - switch between them
* :vsp FILENAME - edit on the right
* \* - will find the current word in the file
* zf - fold, zo - unfold, or click
* C_l - clean console from last search 

cool jumps:
*  f - jump to the character (maybe not so faster than hop.nvim)
*  t - to. e.g. dt) delete to )

-- colorscheme
-- optionally fixing transparency
-- highlight Normal guibg=none
-- highlight NonText guibg=none
-- highlight Normal ctermbg=none
-- highlight NonText ctermbg=none

-- Fix for dracula transparency
-- gl.dracula_colorterm = 0

-- " ron - vs code like, C code is terrible
-- " pablo - green/yellow
-- " codedark - very good! Looks like VS Code Dark
-- " dracula - very good!
-- " molokai - very good! Fake nice monokai, proper cursorline
-- " onehalflight - pastel, supports lightline via: let g:lightline.colorscheme='onehalfdark'
-- " gruvbox - solazired
-- " vim-deep-space - looks kinda cool


```
-- https://github.com/nanotee/nvim-lua-guide/
-- https://github.com/elianiva/dotfiles/tree/master/nvim/.config/nvim
-- https://github.com/beauwilliams/Dotfiles/tree/master/Vim/nvim/lua
-- https://gitlab.com/kflak/dots/-/tree/main/nvim
-- https://oroques.dev/notes/neovim-init/
-- https://www.reddit.com/r/neovim/comments/kfxqcr/how_to_migrate_from_initvim_to_initlua/
-- https://github.com/tjdevries/config_manager/blob/master/xdg_config/nvim/init.lua
-- https://github.com/CalinLeafshade/dots/blob/master/nvim/.config/nvim/lua/leafshade/globals.lua
-- https://github.com/Kethku/vim-config/blob/main/lua/dotfiles/module/settings.lua

## TODO: remake lsp config???

-- https://github.com/neovim/nvim-lspconfig/issues/372
-- https://github.com/LunarVim/LunarVim/blob/d01ba08eaec1640ac2d038893525b3ba0af25813/lua/utils/init.lua
-- https://www.lunarvim.org/languages/#formatting
-- https://www.youtube.com/watch?v=PA7zZNJXJEk

## TODO: coq nvim?

# TODO: LSP and other stuff
https://www.twitch.tv/videos/1190279834    01:47:32
https://www.twitch.tv/videos/1190959702
https://github.com/WaylonWalker/devtainer/tree/main/nvim/.config/nvim/lua/waylonwalker
https://github.com/psycoder01/Neovim-Config/tree/master/lua

```
-
