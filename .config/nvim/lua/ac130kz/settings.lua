-- leader
vim.g.mapleader = " "      -- SpaceEmacs style leader
vim.g.maplocalleader = " " --

-- command completion
vim.o.wildmode = "longest:full,full,list" -- previously: "longest,list,full"

-- general settings
vim.o.number = true                                -- current line number
vim.o.relativenumber = true                        -- relative number for easy navigation
vim.o.background = "dark"                          -- setting dark background just in case
vim.o.termguicolors = true                         -- 24 bit colour support
--vim.o.lazyredraw = true                            -- don't redraw while executing macros
vim.o.cursorline = true                            -- highlight the current line
vim.o.mouse = "a"                                  -- mouse support
vim.o.laststatus = 2                               -- always display the current mode
vim.o.undolevels = 200                             -- a generous amount of undos
vim.o.signcolumn = "yes"                           -- a small column to the left of line numbers
vim.o.showmode = false                             -- mode is not necessary with lightline
vim.o.showmatch = true                             -- matching brackets
vim.o.foldcolumn = "1"                             -- shows a handy column to fold/unfold code with a mouse
vim.o.joinspaces = false                           -- remove unnecessary spaces
vim.o.hidden = true                                -- editing won't work if we edit
vim.o.updatetime = 200                             -- faster updates
vim.o.timeoutlen = 300                             -- faster input
vim.o.backup = false                               -- disables backups
vim.o.writebackup = false                          --
vim.o.swapfile = false                             --
vim.o.wrap = true                                  -- line wrapping
-- vim.o.cmdheight      = 2             -- more space for messages
vim.o.clipboard = vim.o.clipboard .. "unnamedplus" -- simple clipboard support, install xsel
vim.o.shortmess = vim.o.shortmess .. "cC"          -- don't give insertion completion messages
vim.o.foldmethod = "syntax"                        -- syntax based folding
vim.o.foldenable = false                           -- disable folds upon load
--vim.o.confirm = true                               -- confirm to save on exit
vim.o.grepprg = "rg --vimgrep"                     -- faster grep
vim.o.grepformat = "%f:%l:%c:%m"                   --
--vim.o.pumblend = 15 -- popup color blending with pseudo-transparency
vim.o.pumheight = 10                               -- maximum number of entries in a popup, 0 - all the screen space
vim.o.showmode = false                             -- don't show mode since we have a statusline
vim.o.splitkeep = "screen"                         -- keep the text on the same screen line after a split

-- TODO: look into these creatures
-- https://vimhelp.org/change.txt.html#fo-table
-- vim.o.formatoptions = "jcroqlnt" -- tcqj

-- TODO: this one as well
-- vim.o.sessionoptions = { "buffers", "curdir", "tabpages", "winsize" }

-- just for reference, there's this new one
-- vim.o.statuscolumn
-- maybe I should ditch custom statusline for a built-in, or even showcmdloc = "statusline"

-- tabs to spaces, easy indentation handling
vim.o.softtabstop = 4
vim.o.expandtab = true
vim.o.shiftwidth = 4
vim.o.tabstop = 4
vim.o.smartindent = true
vim.o.autoindent = true
vim.o.shiftround = true

-- spelling dictionary: en is a bit weird
vim.o.spelllang = "en_us"

-- styling and blinking the cursor, consider faster blinking
vim.o.guicursor =
"n-v-c:block,i-ci-ve:ver25,r-cr:hor20,o:hor50,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor,sm:block-blinkwait175-blinkoff150-blinkon175"

-- make diffing better https://vimways.org/2018/the-power-of-diff
vim.o.diffopt = "iwhite,algorithm:patience,indent-heuristic"

-- override settings for web
vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
    pattern = {
        " *.js", ".jsx", ".ts", ".tsx", "*.html",
        "*.htm", "*.css", "*.scss", "*.sass", "*.less"
    },
    callback = function()
        vim.bo.softtabstop = 2
        vim.bo.shiftwidth = 2
        vim.bo.tabstop = 2
    end
})

-- highlighted yank
-- TODO: port
-- https://www.reddit.com/r/neovim/comments/gofplz/neovim_has_added_the_ability_to_highlight_yanked/
-- autocmd TextYankPost * silent! lua vim.highlight.on_yank {higroup=(vim.fn['hlexists']('HighlightedyankRegion') > 0 and 'HighlightedyankRegion' or 'IncSearch'), timeout=500}
--
--vim.api.nvim_create_autocmd({"BufNewFile", "BufRead"}, {
--    callback = function ()
--        vim.bo.softtabstop = 2
--        vim.bo.shiftwidth = 2
--        vim.bo.tabstop = 2
--    end
--})

-- colors, wait for lua
vim.cmd [[ colorscheme codedark ]]
--vim.cmd [[ colorscheme tokyonight-night ]]
--vim.cmd [[ colorscheme kanagawa-wave ]]
--vim.cmd [[ colorscheme github_dimmed ]]

-- disable useless plugins
vim.g.loaded_ruby_provider = 0
vim.g.loaded_python_provider = 0
vim.g.loaded_perl_provider = 0
vim.g.loaded_man = 0
vim.g.loaded_gzip = 0
vim.g.loaded_netrwPlugin = 0
vim.g.loaded_tarPlugin = 0
vim.g.loaded_zipPlugin = 0
vim.g.loaded_2html_plugin = 0
vim.g.loaded_remote_plugins = 0
