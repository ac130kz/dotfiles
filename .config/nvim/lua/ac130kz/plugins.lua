-- auto install lazy, keep in mind it uses ssh instead of http
local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        'git',
        'clone',
        '--filter=blob:none',
        'git@github.com:folke/lazy.nvim.git',
        '--branch=stable', -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

-- plugin manager
require('lazy').setup({
        -- lsp provider
        { 'neovim/nvim-lspconfig' },
        { 'nvim-lua/plenary.nvim' },
        { 'nvim-lua/popup.nvim' },
        --{
        --    'williamboman/mason.nvim',
        --    build = ':MasonUpdate' -- updates registry contents
        --},

        -- https://github.com/williamboman/mason.nvim
        --{ 'mfussenegger/nvim-lint' },
        -- also try https://github.com/mhartington/formatter.nvim/blob/master/lua/formatter/filetypes/python.lua
        -- DEAD
        {
            'nvimtools/none-ls.nvim',
            dependencies = { 'nvim-lua/plenary.nvim' }
        }, -- extra code actions, such as formatting

        -- additional tools
        { 'simrat39/rust-tools.nvim' },
        --{ 'akinsho/flutter-tools.nvim', dependencies = { 'nvim-lua/plenary.nvim' } },

        -- lsp status ui
        -- TODO: update once stable
        { 'j-hui/fidget.nvim', branch = 'legacy' },

        -- per project marks
        { 'ThePrimeagen/harpoon' },

        -- lsp syntax parsing - better language support
        {
            'nvim-treesitter/nvim-treesitter',
            build = ':TSUpdate',
            dependencies = {
                -- extra treesitter refactoring module
                'nvim-treesitter/nvim-treesitter-refactor',
            }
        },

        -- cool context info
        -- TODO: remove? It's buggy
        --{ 'code-biscuits/nvim-biscuits' },
        --require('nvim-biscuits').setup({
        --default_config = {
        --    max_length = 30,
        --    min_distance = 5
        --}
        --})

        -- code completion
        {
            'hrsh7th/nvim-cmp',
            dependencies = {
                -- snippets are required
                'dcampos/nvim-snippy',
                'dcampos/cmp-snippy',

                -- plugins
                'hrsh7th/cmp-nvim-lsp',
                'hrsh7th/cmp-buffer',
                'hrsh7th/cmp-path',
                'hrsh7th/cmp-cmdline',
            },
            --event = 'InsertEnter',
        },

        -- show funtion definition
        { 'ray-x/lsp_signature.nvim' },

        -- put closing tags
        { 'windwp/nvim-autopairs' },

        -- error lens
        {
            'glepnir/lspsaga.nvim',
            event = 'LspAttach',
            dependencies = {
                'nvim-tree/nvim-web-devicons',
                --Please make sure you install markdown and markdown_inline parser
                'nvim-treesitter/nvim-treesitter'
            }
        },

        -- comfortable moves, like Vimium within the browser
        { 'phaazon/hop.nvim' },

        -- comment handler
        -- TODO: migrate to numToStr/Comment.nvim
        {
            'terrortylor/nvim-comment',
            dependencies = {
                -- fix for typescript/tsx
                'JoosepAlviste/nvim-ts-context-commentstring',
            }
        },

        -- color schemes
        { 'tomasiser/vim-code-dark' },
        { 'folke/tokyonight.nvim' },
        { 'rebelot/kanagawa.nvim' },
        { 'EdenEast/nightfox.nvim' }, -- TODO: try Carbonfox
        { 'projekt0n/github-nvim-theme', branch = '0.0.x' },

        -- TODO: obsolete? Support for tons of languages and enchancements
        --{ 'sheerun/vim-polyglot' },

        -- bottom line
        -- TODO: move to galaxyline?, watch the devaslife video on lsp for config
        {
            'hoob3rt/lualine.nvim',
            dependencies = {
                { 'nvim-tree/nvim-web-devicons' }
            },
        },

        -- indent guides
        {
            'lukas-reineke/indent-blankline.nvim',
            main = "ibl",
            event = 'VeryLazy',
            opts = {}
        },
        -- colorizing colors, where appropriate
        { 'NvChad/nvim-colorizer.lua' },

        -- nice startup screen
        { 'mhinz/vim-startify',       lazy = false },

        -- navigation plugins
        {
            'nvim-telescope/telescope.nvim',
            dependencies = {
                'nvim-lua/popup.nvim',
                'nvim-lua/plenary.nvim',
                -- plugins:
                -- file manager
                'nvim-telescope/telescope-file-browser.nvim',
                -- nice native search https://www.youtube.com/watch?v=8PnNeIoZwx0
                { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make -j$(nproc)' },
            },
        },

        -- git support
        {
            'lewis6991/gitsigns.nvim',
            dependencies = {
                { 'nvim-lua/plenary.nvim', lazy = false }
            },
        },

        -- tab bar
        {
            'seblj/nvim-tabline',
            dependencies = { 'nvim-tree/nvim-web-devicons' }
        },
    },
    {
        -- too lazy, doesn't load all the plugins
        -- defaults = { lazy = true },
        concurrency = 12,
        git = {
            default_url_format = 'git@github.com:%s.git'
        },
        --install = {
        --    colorscheme = { "codedark" }
        --},
        performance = {
            rtp = {
                disabled_plugins = {
                    --'gzip',
                    --'matchit',
                    --'matchparen',
                    --'netrwPlugin',
                    --'tarPlugin',
                    --'tohtml',
                    --'tutor',
                    --'zipPlugin',
                },
            },
        },
    }
)
