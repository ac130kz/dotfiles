-- toggle spelling
vim.keymap.set("n", "<leader>s", function()
    vim.opt_local.spell = not (vim.opt_local.spell:get())
end, { noremap = true })

-- nerdcommenter ctrl + / comments
-- TODO: migrate to nvimcomment
--local comment = require('nvim_comment')
--.comment_toggle(<line1>, <line2>)")
vim.keymap.set("v", "<C-_>", "<Cmd>'<,'>CommentToggle<CR>", { noremap = true })
vim.keymap.set("n", "<C-_>", "<Cmd>CommentToggle<CR>", { noremap = true })

-- '''easy motion''', there are also other functions and features like hint_char1
-- directions, single line, before/after
local hop = require("hop")
vim.keymap.set("n", "s", hop.hint_char2, { noremap = true })
vim.keymap.set("n", "S", hop.hint_patterns, { noremap = true })

-- keep the cursor centered
vim.keymap.set("n", "n", "nzzzv", { noremap = true })
vim.keymap.set("n", "N", "Nzzzv", { noremap = true })
vim.keymap.set("n", "J", "mzJ`z", { noremap = true })

-- undo breakpoints
local breakpoints = { ",", ".", "[", "(", "<", "{", "]", ")", ">", "}", "!", "?" }
for _, map in ipairs(breakpoints) do
    vim.keymap.set("i", map, map .. "<C-g>u", { noremap = true })
end

-- TODO: moving text: https://github.com/ThePrimeagen/.dotfiles
-- vim.keymap.set("v", ":m", "mzJ`z", { noremap = true })

-- Telescope: find_files, search buffers, so on
local telescope = require("telescope")
local telescope_builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>ff", telescope_builtin.find_files, { noremap = true })
vim.keymap.set("n", "<leader>fg", telescope_builtin.live_grep, { noremap = true })
vim.keymap.set("n", "<leader>fb", telescope_builtin.buffers, { noremap = true })
vim.keymap.set("n", "<leader>fh", telescope_builtin.help_tags, { noremap = true })
-- file browser
vim.keymap.set("n", "<C-b>", function()
    telescope.extensions.file_browser.file_browser() -- TODO: maybe make a separate script to open in the current folder (vim.fn.shellescape(vim.fn.expand("%:p:h")))
end, { noremap = true })

-- handy window movement
vim.keymap.set("n", "<C-h>", "<C-w>h", { noremap = true, silent = true })
vim.keymap.set("n", "<C-j>", "<C-w>j", { noremap = true, silent = true })
vim.keymap.set("n", "<C-k>", "<C-w>k", { noremap = true, silent = true })
vim.keymap.set("n", "<C-l>", "<C-w>l", { noremap = true, silent = true })

-- move between tabs
vim.keymap.set("n", "<leader>1", "1gt", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>2", "2gt", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>3", "3gt", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>4", "4gt", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>5", "5gt", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>6", "6gt", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>7", "7gt", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>8", "8gt", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>9", "9gt", { noremap = true, silent = true })

-- do not wipe the buffer when pasting
vim.keymap.set("x", "<leader>p", "\"_dP", { noremap = true })
