function _G.LatexCompiler()
    if vim.fn.expand("%:e") ~= "tex" then
        print("ERROR: Not a .tex file")
        return
    end

    -- xelatex produces smaller pdfs, but it's
    -- worse than pdflatex with packages
    local compiler = "pdflatex"
    -- bibtex is also better with other packages
    -- while biber is very good at configuring
    local bibliography = "biber"

    if not vim.fn.executable(compiler) or not vim.fn.executable(bibliography) then
        print("ERROR: LaTeX compiler or bibliography handler not found")
        return
    end

    --local file = vim.fn.shellescape(vim.fn.expand("%:p"))
    local dir = vim.fn.shellescape(vim.fn.expand("%:p:h"))
    local base = vim.fn.shellescape(vim.fn.expand("%:p:r"))
    local compiler_runner = compiler .. " --output-directory=" .. dir .. " " .. base
    local biblio_runner = bibliography .. " --input-directory=" .. dir .. " " .. base
    --print(biblio_runner)

    -- for whatever reason it's recommended to run the compiler at least twice
    vim.fn.system(compiler_runner .. " && " .. biblio_runner .. " && " .. compiler_runner .. " && " .. compiler_runner)
end

-- TODO: fix :w! \|
vim.keymap.set("n", "<leader>c", LatexCompiler, { noremap = true, silent = true })

-------------- Latex compiler
-- TODO: finish
-- function! LatexCompiler()
-- if (expand('%:e') != "tex")
-- echo "ERROR: Not a .tex file!"
-- return
-- endif
-- let l:compiler = "xelatex"
-- let l:bibliography = "biber"
-- if (!executable(l:compiler) || !executable(l:bibliography))
-- echo "ERROR: pdflatex|xelatex/biber|bibtex not found in system PATH!"
-- return
-- endif
-- let l:file = shellescape(expand('%:p'))
-- let l:dir = shellescape(expand('%:p:h'))
-- let l:base = shellescape(expand('%:p:r'))
-- let l:compiler_runner = l:compiler . " --output-directory=" . l:dir . " " . l:base
-- let l:biblio_runner = l:bibliography . " --input-directory=" . l:dir . " " . l:base
-- " for whatever reason it's recommended to run the compiler at least twice
-- silent echo system(l:compiler_runner . " && " . l:biblio_runner . " && " . l:compiler_runner . " && " . l:compiler_runner)
-- endfunction
-- " compile Latex documents
-- map <leader>c :w! \| :call LatexCompiler() <CR>
