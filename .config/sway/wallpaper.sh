#!/bin/sh
BG_PATH=~/.config/sway/wallpaper.jpg
TIMEOUT=4s

# applying the old one
#feh --bg-scale $BG_PATH

# network access timeout
sleep $TIMEOUT

# generating a selection between bing and unsplash, then fetching
RNG=$(od -A n -t d -N 1 /dev/urandom)
if [ $(( $RNG % 2 )) -eq 0 ]
then
    curl -s "https://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=en-US" | curl "https://bing.com$(jq -r ".images[0].url")" -o $BG_PATH
else
    wget "https://source.unsplash.com/3840x2160/?nature,featured" -O $BG_PATH
fi

# setting the new one
#feh --bg-scale $BG_PATH
#feh --bg-scale "~/Downloads/Windows\ XP\ 2020.jpg"
