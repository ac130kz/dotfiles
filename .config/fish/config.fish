# If not running interactively, don't do anything
if not status is-interactive
    exit
end

### Basic setup
# fish completion
if test -f /usr/share/fish/completions
    source /usr/share/fish/completions
end

# disable greeting
set -g fish_greeting

# git prompt
set -g __fish_git_prompt_showupstream auto
set -g __fish_git_prompt_showuntrackedfiles
set -g __fish_git_prompt_showdirtystate
set -g __fish_git_prompt_use_informative_chars
set -g __fish_git_prompt_showcolorhints

# prompt
printf '%s %s$' $PWD (fish_git_prompt)

# colours
# https://gitlab.com/dwt1/dotfiles
set fish_color_normal brcyan
set fish_color_autosuggestion '#7d7d7d'
set fish_color_command brcyan
set fish_color_error '#ff6c6b'
set fish_color_param brcyan

### Global variables
set -gx DOTNET_CLI_TELEMETRY_OPTOUT 1
set -gx _JAVA_AWT_WM_NONREPARENTING 1
set -gx RUSTFLAGS "-C opt-level=3 -C target-cpu=native"
set -gx PATH "$HOME/.cargo/bin:$HOME/.local/bin:$PATH"
set -gx EDITOR "nvim"
set -gx VISUAL "nvim"
set -gx SUDO_EDITOR "nvim"
set -gx USE_BLAS64 1
set -gx JULIA_NUM_THREADS 12

# Wayland
set -gx QT_QPA_PLATFORMTHEME qt6ct
set -gx CLUTTER_BACKEND wayland
set -gx SDL_VIDEODRIVER wayland
set -gx XDG_SESSION_TYPE wayland
set -gx XDG_CURRENT_DESKTOP sway
set -gx QT_QPA_PLATFORM wayland
set -gx QT_WAYLAND_DISABLE_WINDOWDECORATION 1
set -gx MOZ_ENABLE_WAYLAND 1
set -gx ECORE_EVAS_ENGINE wayland_egl
set -gx ELM_ENGINE wayland_egl

# Graphics:
# 1) option Intel
#set -gx LIBVA_DRIVER_NAME iHD
#set -gx VDPAU_DRIVER va_gl
#set -gx __EGL_VENDOR_LIBRARY_FILENAMES "/usr/share/glvnd/egl_vendor.d/50_mesa.json"

# 2) option Nvidia
set -gx VDPAU_DRIVER nvidia
set -gx LIBVA_DRIVER_NAME nvidia
set -gx GBM_BACKEND nvidia-drm
set -gx __GLX_VENDOR_LIBRARY_NAME nvidia
# too explicit, but may fix the issues
set -gx __EGL_VENDOR_LIBRARY_FILENAMES "/usr/share/glvnd/egl_vendor.d/10_nvidia.json"
# TODO: remove when fixed in sway
set -gx WLR_NO_HARDWARE_CURSORS 1

# Firefox temporary Vaapi fixes, requires: libva-nvidia-driver
set -gx MOZ_DISABLE_RDD_SANDBOX 1
set -gx NVD_BACKEND direct

# Sway general
set -gx WLR_RENDERER vulkan
set -gx __GL_GSYNC_ALLOWED 0
set -gx __GL_VRR_ALLOWED 0
# TODO: remove, probably already fixed
#set -gx WLR_DRM_NO_MODIFIERS 1

### Aliases
alias cool_history "history | sk"
alias ls "ls --group-directories-first --color=auto"
alias nano "nano -l"
alias grep "grep --color=auto"
alias enable_wifi "nmcli radio wifi on"
alias disable_wifi "nmcli radio wifi off"
alias optimize_png 'find ./ -type f \( -iname \*.png \) -print0 | parallel -0 --bar ect -9 -strip {} \;'
alias optimize_jpg 'find ./ -type f \( -iname \*.jp\*g \) -print0 | parallel -0 --bar jpegoptim -t --all-progressive --strip-all {} \;'
alias drop_caches "echo 3 | doas tee /proc/sys/vm/drop_caches"

# Gdrive: requires rclone
alias gdrive_up "rclone mount --daemon gdrive:/My\ files $HOME/gdrive"
alias gdrive_down "sudo sync && fusermount -u $HOME/gdrive"

# OpenVPN example
# alias test_vpn "sudo openvpn --config $HOME/test.ovpn --auth-user-pass $HOME/test-openvpn-creds.txt"

### Functions
function play_video
    if test -z $argv[1]
        echo 'Usage: play_video "https://youtube.com/watch?v=fffffff"'
        return
    end

    mpv --really-quiet --no-input-terminal $argv[1] &
    disown
end

function play_music
    # --no-video
    mpv --shuffle --input-ipc-server=/tmp/mpvsocket --really-quiet --no-input-terminal "https://www.youtube.com/playlist?list=PLKsUrD5RbYoLyWXxsxWj6vU98swwpsOac" &
    disown
end

function update_crates
    rustup update
    cargo install broot protobuf-codegen skim diesel_cli flamegraph hyperfine kickoff nu cargo-nextest py-spy nixpkgs-fmt gitui speedtest-rs cornucopia refinery_cli
    # fd
    # swayr 
    # persway
    #  du-dust
    #  coreutils
    #  rtx-cli
    # exa zellij 
    # prqlc arti
    # sqlx-cli
    #  gitoxide nixpacks swayrbar
end

# a wrapper around makepkg to use local repo
function make_pkgbuild
    paru -Bi .
end

# handle !! just like bash
function last_history_item; echo $history[1]; end
abbr -a !! --position anywhere --function last_history_item

# system ui
if test (tty) = "/dev/tty1"
    # ssh agent
    # https://gist.github.com/josh-padnick/c90182be3d0e1feb89afd7573505cab3
    if test -z (pgrep -x ssh-agent | string collect)
        eval (ssh-agent -c) > /dev/null
        set -Ux SSH_AUTH_SOCK $SSH_AUTH_SOCK
        set -Ux SSH_AGENT_PID $SSH_AGENT_PID
    end

    # start sway
    pgrep -x sway || exec dbus-run-session sway --unsupported-gpu 

    # add logging
    # > /var/log/sway.log 2>&1
end

# kitty integration
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/fish/vendor_conf.d/kitty-shell-integration.fish"
    source "$KITTY_INSTALLATION_DIR/shell-integration/fish/vendor_conf.d/kitty-shell-integration.fish"
end

# pnpm integration
set -gx PNPM_HOME "/home/user/.local/share/pnpm"
set -gx PATH "$PNPM_HOME" $PATH
