## How to disable mini-window

Edit ~/.config/zoomus.conf and change the value enableMiniWindow=true to enableMiniWindow=false, and restart zoom.

## Sharing on Wayland via Pipewire
enableWaylandShare=false
