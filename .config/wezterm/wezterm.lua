local wezterm = require 'wezterm';

return {
  enable_tab_bar = false,
  font = wezterm.font_with_fallback({
    "JetBrains Mono",
    "Font Awesome 6 Free"
  }),
  window_padding = {
    left = 0,
    right = 0,
    top = 0,
    bottom = 0,
  },
  font_size = 10,
  color_scheme = "Pro",
  colors = {
    -- The default text color
    foreground = "white",
    -- The default background color
    background = "black",
  },
}
