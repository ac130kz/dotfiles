local wezterm = require 'wezterm';

return {
    font = wezterm.font("JetBrains Mono", {dpi = 141}), -- Fira Code
    font_size = 10,
    --color_scheme = "Batman",
    colors = {
      -- The default text color
      foreground = "white",
      -- The default background color
      background = "black",
    },
    enable_wayland = true,
}
