// Mozilla User Preferences
// https://searchfox.org/mozilla-release/source/modules/libpref/init/all.js

// Accessibility
user_pref("accessibility.force_disabled", 1);
user_pref("accessibility.typeaheadfind.flashBar", 0);

// Telemetry
user_pref("app.normandy.api_url", "");
user_pref("app.normandy.enabled", false);
user_pref("app.normandy.first_run", false);
user_pref("app.normandy.user_id", "");
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("app.update.auto", false); // Use system updater
user_pref("beacon.enabled", false);
user_pref("breakpad.reportURL", "");

// Some user UI prefs
user_pref("browser.aboutConfig.showWarning", false);
user_pref("browser.aboutwelcome.protonDesign", false);
user_pref("browser.bookmarks.restore_default_bookmarks", false);

// Cache
user_pref("browser.cache.disk.capacity", 3145728); // just in case it's enabled below
user_pref("browser.cache.disk.enable", false);
user_pref("browser.cache.disk.preload_chunk_count", 6);
user_pref("browser.cache.memory.capacity", 2359296);
user_pref("browser.cache.memory.max_entry_size", 10000);

// Main settings
user_pref("browser.compactmode.show", true);
user_pref("browser.contentblocking.category", "custom"); // Content blocking
user_pref("browser.contentblocking.report.hide_vpn_banner", true);
user_pref("browser.contentblocking.report.lockwise.enabled", false);
user_pref("browser.contentblocking.report.show_mobile_app", false);
user_pref("browser.contentblocking.report.vpn.enabled", false);
user_pref("browser.ctrlTab.sortByRecentlyUsed", true);
user_pref("browser.discovery.containers.enabled", false); // Disable recommendations
user_pref("browser.discovery.enabled", false);
user_pref("browser.download.autohideButton", false);
user_pref("browser.download.manager.addToRecentDocs", false);
user_pref("browser.download.panel.shown", true);
user_pref("browser.download.save_converter_index", 0); // Save full html page
user_pref("browser.eme.ui.firstContentShown", true);
user_pref("browser.firefox-view.feature-tour", "{\"message\":\"FIREFOX_VIEW_FEATURE_TOUR\",\"screen\":\"\",\"complete\":true}");
user_pref("browser.fixup.alternate.prefix", ""); // Don't apply url rewrites
user_pref("browser.fixup.alternate.suffix", "");
user_pref("browser.formfill.enable", false);
user_pref("browser.gesture.swipe.left", "");
user_pref("browser.gesture.swipe.right", "");
user_pref("browser.link.open_newwindow.restriction", 0); // Ignore page rules for a new window
user_pref("browser.messaging-system.whatsNewPanel.enabled", false);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false);
user_pref("browser.newtabpage.activity-stream.feeds.telemetry", false);
user_pref("browser.newtabpage.activity-stream.feeds.topsites", false);
user_pref("browser.newtabpage.activity-stream.improvesearch.topSiteSearchShortcuts.havePinned", "");
user_pref("browser.newtabpage.activity-stream.showSearch", false);
user_pref("browser.newtabpage.activity-stream.showSponsored", false);
user_pref("browser.newtabpage.activity-stream.showSponsoredTopSites", false);
user_pref("browser.newtabpage.activity-stream.telemetry", false);
user_pref("browser.newtabpage.activity-stream.telemetry.structuredIngestion.endpoint", "");
user_pref("browser.newtabpage.pinned", "[]");
user_pref("browser.pageActions.persistedActions", "{\"version\":1,\"ids\":[],\"idsInUrlbar\":[],\"idsInUrlbarPreProton\":[]}");
user_pref("browser.ping-centre.telemetry", false);
user_pref("browser.privatebrowsing.forceMediaMemoryCache", true);
user_pref("browser.protections_panel.infoMessage.seen", true);
user_pref("browser.proton.enabled", false); // Disable new browser styling
user_pref("browser.region.network.url", "");
user_pref("browser.region.update.enabled", false);
user_pref("browser.rights.3.shown", true);

// Safebrowsing
user_pref("browser.safebrowsing.blockedURIs.enabled", false);
user_pref("browser.safebrowsing.downloads.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.block_dangerous", false);
user_pref("browser.safebrowsing.downloads.remote.block_dangerous_host", false);
user_pref("browser.safebrowsing.downloads.remote.block_potentially_unwanted", false);
user_pref("browser.safebrowsing.downloads.remote.block_uncommon", false);
user_pref("browser.safebrowsing.downloads.remote.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.timeout_ms", 0);
user_pref("browser.safebrowsing.downloads.remote.url", "");
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);
user_pref("browser.safebrowsing.provider.google.gethashURL", "");
user_pref("browser.safebrowsing.provider.google.updateURL", "");
user_pref("browser.safebrowsing.provider.google4.dataSharingURL", "");
user_pref("browser.safebrowsing.provider.google4.gethashURL", "");
user_pref("browser.safebrowsing.provider.google4.updateURL", "");

// Search
user_pref("browser.search.hiddenOneOffs", "Google");
user_pref("browser.search.region", "KZ");
user_pref("browser.search.separatePrivateDefault.urlbarResult.enabled", false);
user_pref("browser.search.suggest.enabled.private", true);

// Disable new extra session saving
user_pref("browser.sessionstore.persist_closed_tabs_between_sessions", false);
user_pref("browser.sessionstore.closedTabsFromClosedWindows", false);

// Some tabs, startup settings
user_pref("browser.shell.checkDefaultBrowser", true);
user_pref("browser.shell.shortcutFavicons", false);
user_pref("browser.ssl_override_behavior", 1);
user_pref("browser.startup.homepage_override.mstone", "ignore");
user_pref("browser.suppress_first_window_animation", false);
user_pref("browser.tabs.crashReporting.sendReport", false);
user_pref("browser.tabs.inTitlebar", 0);
user_pref("browser.theme.toolbar-theme", 0);
user_pref("browser.toolbars.bookmarks.visibility", "never");
user_pref("browser.topsites.useRemoteSetting", false);
user_pref("browser.touchmode.auto", false);
user_pref("browser.translation.engine", "");
user_pref("browser.translations.enable", false);

// UI state
user_pref("browser.uidensity", 1);
user_pref("browser.uitour.enabled", false);
user_pref("browser.uitour.url", "");

// Url bar
user_pref("browser.urlbar.keepPanelOpenDuringImeComposition", true);
user_pref("browser.urlbar.placeholderName", "Google");
user_pref("browser.urlbar.placeholderName.private", "Google");
user_pref("browser.urlbar.quicksuggest.scenario", "history"); // offline uses Wiki
user_pref("browser.urlbar.shortcuts.bookmarks", false);
user_pref("browser.urlbar.shortcuts.history", false);
user_pref("browser.urlbar.shortcuts.tabs", false);
user_pref("browser.urlbar.suggest.bookmark", false);
user_pref("browser.urlbar.suggest.calculator", true);
user_pref("browser.urlbar.suggest.engines", false);
user_pref("browser.urlbar.suggest.history", false);
user_pref("browser.urlbar.suggest.openpage", false);
user_pref("browser.urlbar.suggest.topsites", false);
user_pref("browser.urlbar.trimURLs", false);

// XUL
user_pref("browser.xul.error_pages.expert_bad_cert", true);

// Datareporting
user_pref("datareporting.healthreport.infoURL", "");
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);

// Devtools
user_pref("devtools.aboutdebugging.collapsibilities.processes", false);
user_pref("devtools.accessibility.enabled", false);
user_pref("devtools.application.enabled", false);
user_pref("devtools.debugger.dom-mutation-breakpoints-visible", true);
user_pref("devtools.debugger.event-listeners-visible", true);
user_pref("devtools.debugger.skip-pausing", true);
user_pref("devtools.everOpened", true);
user_pref("devtools.inspector.activeSidebar", "ruleview");
user_pref("devtools.inspector.selectedSidebar", "ruleview");
user_pref("devtools.inspector.three-pane-enabled", false);
user_pref("devtools.memory.enabled", false);
user_pref("devtools.netmonitor.msg.visibleColumns", "[\"data\",\"time\"]");
user_pref("devtools.remote.adb.extensionURL", "");
user_pref("devtools.responsive.reloadNotification.enabled", false);
user_pref("devtools.styleeditor.enabled", false);
user_pref("devtools.theme", "dark");
user_pref("devtools.toolbox.host", "right");
user_pref("devtools.toolbox.previousHost", "bottom");
user_pref("devtools.toolbox.selectedTool", "netmonitor");
user_pref("devtools.toolbox.splitconsoleEnabled", true);
user_pref("devtools.toolbox.tabsOrder", "inspector,webconsole,netmonitor,storage,jsdebugger,performance");
user_pref("devtools.webconsole.input.editorOnboarding", false);

// Rollout skips
user_pref("distribution.archlinux.bookmarksProcessed", true); // Ignore processing
user_pref("doh-rollout.disable-heuristics", true);
user_pref("doh-rollout.doneFirstRun", true);
user_pref("doh-rollout.home-region", "KZ");

// Battery
user_pref("dom.battery.enabled", false);

// DOM settings
user_pref("dom.disable_window_move_resize", true);
user_pref("dom.enable_web_task_scheduling", true);
user_pref("dom.event.contextmenu.enabled", false); // ignore website blocking access to context menu

// Gamepad
user_pref("dom.gamepad.enabled", false);
user_pref("dom.gamepad.extensions.enabled", false);

// indexedDB
user_pref("dom.indexedDB.logging.details", false);
user_pref("dom.indexedDB.logging.enabled", false);

// Core CPU counts
user_pref("dom.ipc.processPrelaunch.fission.number", 6);
user_pref("dom.maxHardwareConcurrency", 12);

// More DOM settings
user_pref("dom.popup_allowed_events", "click dblclick mousedown pointerdown"); // ignore other methods
user_pref("dom.push.connection.enabled", false);
user_pref("dom.push.enabled", false);
user_pref("dom.push.userAgentID", "");

// Https only
user_pref("dom.security.https_only_mode", true);
user_pref("dom.security.https_only_mode_ever_enabled", true);
user_pref("dom.security.unexpected_system_load_telemetry_enabled", false);

// WebGPU
user_pref("dom.webgpu.enabled", true);

// Vibrator
user_pref("dom.vibrator.enabled", false);

// Notifications
user_pref("dom.webnotifications.enabled", false);
user_pref("dom.webnotifications.serviceworker.enabled", false);

// Extensions
user_pref("extensions.activeThemeID", "{5f3d72a6-5ae0-4d03-8bf4-21f2b0ab1010}");
user_pref("extensions.formautofill.creditCards.enabled", false);
user_pref("extensions.fxmonitor.firstAlertShown", true);
user_pref("extensions.getAddons.cache.enabled", false);
user_pref("extensions.htmlaboutaddons.recommendations.enabled", false);
user_pref("extensions.pictureinpicture.enable_picture_in_picture_overrides", true);
user_pref("extensions.pocket.enabled", false);
user_pref("extensions.postDownloadThirdPartyPrompt", false);
user_pref("extensions.privatebrowsing.notification", true);
user_pref("extensions.quarantinedDomains.enabled", false);
user_pref("extensions.reset_default_search.runonce.1", true);
user_pref("extensions.reset_default_search.runonce.3", true);
user_pref("extensions.reset_default_search.runonce.reason", "previousRun");
user_pref("extensions.ui.dictionary.hidden", true);
user_pref("extensions.ui.extension.hidden", false);
user_pref("extensions.ui.locale.hidden", true);
user_pref("extensions.ui.plugin.hidden", false);
user_pref("extensions.ui.sitepermission.hidden", true);
user_pref("extensions.update.interval", 43200);
user_pref("extensions.webcompat-reporter.newIssueEndpoint", "");
user_pref("extensions.webcompat.enable_picture_in_picture_overrides", true);
user_pref("extensions.webcompat.enable_shims", true);
user_pref("extensions.webcompat.perform_injections", true);
user_pref("extensions.webcompat.perform_ua_overrides", true);
user_pref("extensions.webextensions.restrictedDomains", "");

// Findbar
user_pref("findbar.highlightAll", true);

// Fonts
user_pref("font.default.x-cyrillic", "sans-serif");
user_pref("font.default.x-western", "sans-serif");
user_pref("font.language.group", "x-western");
user_pref("font.name.monospace.x-cyrillic", "JetBrains Mono");
user_pref("font.name.monospace.x-western", "JetBrains Mono");
user_pref("font.name.sans-serif.x-cyrillic", "Lato");
user_pref("font.name.sans-serif.x-western", "Lato");
user_pref("font.name.serif.x-cyrillic", "Noto Serif");
user_pref("font.name.serif.x-western", "Noto Serif");
user_pref("font.size.variable.x-cyrillic", 17);

// Fullscreen timings
user_pref("full-screen-api.transition.timeout", 500);
user_pref("full-screen-api.warning.delay", 200);
user_pref("full-screen-api.warning.timeout", 1500);

// Scheme handlers
user_pref("gecko.handlerService.schemes.irc.0.name", "");
user_pref("gecko.handlerService.schemes.irc.0.uriTemplate", "");
user_pref("gecko.handlerService.schemes.ircs.0.name", "");
user_pref("gecko.handlerService.schemes.ircs.0.uriTemplate", "");
user_pref("gecko.handlerService.schemes.mailto.0.name", "");
user_pref("gecko.handlerService.schemes.mailto.0.uriTemplate", "");
user_pref("gecko.handlerService.schemes.mailto.1.name", "");
user_pref("gecko.handlerService.schemes.mailto.1.uriTemplate", "");

// Scrolling
user_pref("general.autoScroll", true);
user_pref("general.smoothScroll", false);
user_pref("general.smoothScroll.currentVelocityWeighting", "0.1");
user_pref("general.smoothScroll.mouseWheel.migrationPercent", 0);
// if the scrolling distance is small, not as good as windows, may require some tweaking
// user_pref("mousewheel.min_line_scroll_amount", 40);

// Geolocation
user_pref("geo.enabled", false);
user_pref("geo.provider-country.network.url", "");
user_pref("geo.provider.network.url", "");

// Gestures
user_pref("gestures.enable_single_finger_input", false);

// GFX
user_pref("gfx.canvas.accelerated", true);
user_pref("gfx.canvas.accelerated.cache-items", 32768);
user_pref("gfx.canvas.accelerated.cache-size", 4096);
user_pref("gfx.content.skia-font-cache-size", 24);
user_pref("gfx.downloadable_fonts.fallback_delay", 0);
user_pref("gfx.downloadable_fonts.fallback_delay_short", 0);
user_pref("gfx.font_loader.delay", 0);
user_pref("gfx.webrender.all", true);
user_pref("gfx.webrender.compositor", true);
user_pref("gfx.webrender.compositor.force-enabled", true); // <- this one is controversial, works without it
user_pref("gfx.webrender.enabled", true);
user_pref("gfx.webrender.precache-shaders", true);
user_pref("gfx.webrender.program-binary", true);
user_pref("gfx.webrender.program-binary-disk", true);

// Firefox account
user_pref("identity.fxaccounts.auth.uri", "");
user_pref("identity.fxaccounts.enabled", false);
user_pref("identity.fxaccounts.pairing.enabled", false);
user_pref("identity.fxaccounts.remote.pairing.uri", "");

// Media performance and features
user_pref("image.decode-immediately.enabled", true);
user_pref("image.jxl.enabled", true);
user_pref("layers.acceleration.force-enabled", true); // increases CPU usage though
user_pref("layers.gpu-process.enabled", true); // utilize an OOP renderer
user_pref("layers.gpu-process.force-enabled", true);
user_pref("layout.css.grid-template-masonry-value.enabled", true);
user_pref("layout.css.has-selector.enabled", true);
user_pref("layout.css.prefers-color-scheme.content-override", 0); // dark
user_pref("layout.css.report_errors", false);
user_pref("layout.css.touch_action.enabled", false);
user_pref("media.autoplay.default", 5);
user_pref("media.av1.enabled", false);
user_pref("media.eme.enabled", true);
user_pref("media.ffmpeg.vaapi.enabled", true);
user_pref("media.ffvpx.enabled", false); // slow software
user_pref("media.gpu-process-decoder", true);
user_pref("media.hardware-video-decoding.force-enabled", true);
user_pref("media.hardwaremediakeys.enabled", false);
user_pref("media.memory_cache_max_size", 65536);
user_pref("media.peerconnection.ice.default_address_only", true);
user_pref("media.peerconnection.ice.no_host", true);
user_pref("media.peerconnection.ice.proxy_only_if_behind_proxy", true);
user_pref("media.rdd-ffvpx.enabled", false);
user_pref("media.rdd-vpx.enabled", false);
user_pref("media.videocontrols.picture-in-picture.enabled", false);
user_pref("media.videocontrols.picture-in-picture.video-toggle.enabled", false);
user_pref("media.webspeech.synth.enabled", false);

// Narration
user_pref("narrate.enabled", false);

// Network settings
user_pref("network.IDN_show_punycode", true);
user_pref("network.buffer.cache.count", 128);
user_pref("network.buffer.cache.size", 262144);
user_pref("network.captive-portal-service.enabled", false);
user_pref("network.connectivity-service.DNSv4.domain", "google.com");
user_pref("network.connectivity-service.DNSv6.domain", "google.com");
user_pref("network.cookie.cookieBehavior", 1);
user_pref("network.cookie.sameSite.noneRequiresSecure", true);
user_pref("network.dns.disableIPv6", true);
user_pref("network.dns.disablePrefetchFromHTTPS", false);
user_pref("network.dnsCacheEntries", 5000);

// Network performace
user_pref("network.http.accept-encoding", "gzip, deflate, br");
user_pref("network.http.connection-retry-timeout", 200);
user_pref("network.http.connection-timeout", 70);
user_pref("network.http.max-persistent-connections-per-server", 64);

// Pacing 
user_pref("network.http.pacing.requests.min-parallelism", 8);
user_pref("network.http.pacing.requests.burst", 12);

// Limits
user_pref("network.http.referer.XOriginTrimmingPolicy", 2);
user_pref("network.http.speculative-parallel-limit", 16);
user_pref("network.http.tailing.enabled", false);

// Predictor
user_pref("network.predictor.enable-hover-on-ssl", true);
user_pref("network.predictor.enable-prefetch", true);
user_pref("network.predictor.preconnect-min-confidence", 20);
user_pref("network.predictor.prefetch-force-valid-for", 3600);
user_pref("network.predictor.prefetch-min-confidence", 30);
user_pref("network.predictor.prefetch-rolling-load-count", 120);
user_pref("network.predictor.preresolve-min-confidence", 10);

// Proxy settings
user_pref("network.proxy.socks", "localhost");
user_pref("network.proxy.socks_port", 1080);
user_pref("network.proxy.socks_remote_dns", true);

// More security options
user_pref("network.security.esni.enabled", true);
user_pref("network.ssl_tokens_cache_capacity", 32768);

// DoH
user_pref("network.trr.blocklist_cleanup_done", true);
user_pref("network.trr.confirmationNS", "google.com");
user_pref("network.trr.confirmation_telemetry_enabled", false);
user_pref("network.trr.custom_uri", "https://dns.nextdns.io/28a78e");
user_pref("network.trr.mode", 2);
user_pref("network.trr.uri", "https://dns.nextdns.io/28a78e");

// PDFJS
user_pref("pdfjs.enabledCache.state", false); // download first
user_pref("pdfjs.previousHandler.alwaysAskBeforeHandling", true);
user_pref("pdfjs.previousHandler.preferredAction", 4);

// Permissions
user_pref("permissions.default.desktop-notification", 2);
user_pref("permissions.default.geo", 2);
user_pref("permissions.default.xr", 2);
user_pref("permissions.delegation.enabled", false);
user_pref("permissions.manager.defaultsUrl", "");

// Privacy and post session cleanup
user_pref("places.history.enabled", false);
user_pref("pref.advanced.proxies.disable_button.reload", false);
user_pref("pref.general.disable_button.default_browser", false);
user_pref("pref.privacy.disable_button.cookie_exceptions", false);
user_pref("pref.privacy.disable_button.tracking_protection_exceptions", false);
user_pref("privacy.annotate_channels.strict_list.enabled", true);
user_pref("privacy.clearOnShutdown.cookies", false);
user_pref("privacy.clearOnShutdown.offlineApps", true);
user_pref("privacy.clearOnShutdown.sessions", false);
user_pref("privacy.donottrackheader.enabled", true); // DNT
user_pref("privacy.history.custom", true);
user_pref("privacy.popups.showBrowserMessage", false);
user_pref("privacy.purge_trackers.date_in_cookie_database", "0");
user_pref("privacy.query_stripping.enabled", true);
user_pref("privacy.query_stripping.strip_list", "__hsfp __hssc __hstc __s _hsenc _openstat dclid fbclid gbraid gclid hsCtaTracking igshid mc_eid ml_subscriber ml_subscriber_hash msclkid oft_c oft_ck oft_d oft_id oft_ids oft_k oft_lk oft_sk oly_anon_id oly_enc_id rb_clickid s_cid twclid vero_conv vero_id wbraid wickedid yclid"); // Merged with Brave ones
user_pref("privacy.sanitize.pending", "[{\"id\":\"shutdown\",\"itemsToClear\":[\"cache\",\"offlineApps\",\"history\",\"formdata\",\"downloads\"],\"options\":{}},{\"id\":\"newtab-container\",\"itemsToClear\":[],\"options\":{}}]"); // custom cleanup settings
user_pref("privacy.sanitize.sanitizeOnShutdown", true);
user_pref("privacy.sanitize.timeSpan", 0); // all timeline
user_pref("privacy.trackingprotection.enabled", true);
user_pref("privacy.trackingprotection.socialtracking.enabled", true);
user_pref("privacy.webrtc.legacyGlobalIndicator", false);
user_pref("reader.parse-on-load.enabled", false);
user_pref("screenshots.browser.component.enabled", true);

// Security
user_pref("security.OCSP.require", true);
user_pref("security.app_menu.recordEventTelemetry", false);
user_pref("security.cert_pinning.enforcement_level", 2);
user_pref("security.certerrors.recordEventTelemetry", false);
user_pref("security.disable_button.openCertManager", false);
user_pref("security.dialog_enable_delay", 500);
user_pref("security.family_safety.mode", 0);
user_pref("security.identitypopup.recordEventTelemetry", false);
user_pref("security.insecure_connection_text.enabled", true);
user_pref("security.mixed_content.block_display_content", true);
user_pref("security.pki.crlite_mode", 2);
user_pref("security.pki.mitm_canary_issuer", "E=none,CN=None,OU=NA,O=\"None, LLC\",L=Dallas,ST=Texas,C=US");
user_pref("security.protectionspopup.recordEventTelemetry", false);
user_pref("security.remote_settings.crlite_filters.enabled", true);
// user_pref("security.ssl.disable_session_identifiers", true); // slow, private aaaand borked keep-alive
user_pref("security.ssl.require_safe_negotiation", true);
user_pref("security.ssl.treat_unsafe_negotiation_as_broken", true);
user_pref("security.tls.enable_0rtt_data", true);

// Block weak encryption
user_pref("security.ssl3.deprecated.rsa_des_ede3_sha", false);
user_pref("security.ssl3.ecdhe_ecdsa_aes_128_sha", false);
user_pref("security.ssl3.ecdhe_ecdsa_aes_256_sha", false);
user_pref("security.ssl3.ecdhe_rsa_aes_128_sha", false);
user_pref("security.ssl3.ecdhe_rsa_aes_256_sha", false);
user_pref("security.ssl3.rsa_aes_128_gcm_sha256", false);
user_pref("security.ssl3.rsa_aes_128_sha", false);
user_pref("security.ssl3.rsa_aes_256_gcm_sha384", false);
user_pref("security.ssl3.rsa_aes_256_sha", false);

// Services
user_pref("services.sync.clients.lastSync", "0");
user_pref("services.sync.declinedEngines", "");
user_pref("services.sync.engine.passwords", false);
user_pref("services.sync.globalScore", 0);
user_pref("services.sync.nextSync", 0);
user_pref("services.sync.tabs.lastSync", "0");

// Autofill and password management
user_pref("signon.autofillForms", false);
user_pref("signon.formlessCapture.enabled", false);
user_pref("signon.generation.enabled", false);
user_pref("signon.importedFromSqlite", true);
user_pref("signon.management.page.breach-alerts.enabled", false);
user_pref("signon.passwordEditCapture.enabled", false);
user_pref("signon.rememberSignons", false);
user_pref("signon.usage.hasEntry", false);

// Startup page
user_pref("startup.homepage_override_url", "about:blank");
user_pref("startup.homepage_welcome_url", "about:blank");

// Telemetry
user_pref("toolkit.coverage.endpoint.base", "");
user_pref("toolkit.crashreporter.infoURL", "");
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("toolkit.telemetry.bhrPing.enabled", false);
user_pref("toolkit.telemetry.cachedClientID", "");
user_pref("toolkit.telemetry.firstShutdownPing.enabled", false);
user_pref("toolkit.telemetry.newProfilePing.enabled", false);
user_pref("toolkit.telemetry.previousBuildID", "");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("toolkit.telemetry.server", "");
user_pref("toolkit.telemetry.server_owner", "");
user_pref("toolkit.telemetry.shutdownPingSender.enabled", false);
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.updatePing.enabled", false);
user_pref("trailhead.firstrun.didSeeAboutWelcome", true);

// Minor extras
user_pref("webchannel.allowObject.urlWhitelist", "");

// Webgl
user_pref("webgl.force-enabled", true);

// Wayland
user_pref("widget.dmabuf.force-enabled", true);
user_pref("widget.wayland-dmabuf-vaapi.enabled", true);

// Windows only
//user_pref("layers.geometry.d3d11.enabled", true);
//user_pref("media.wmf.dxva.d3d11.enabled", true);

