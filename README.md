# dotfiles

![](screenshot.png)

## System
* Arch Linux
* Wayland
* Sway + i3status
* Nvidia blob driver (optionally available Intel configs)

## Structure
* `configs/` all the extra configs that have to particular folder
* `etc/` the advice there should be used to modify `/etc` on the target system
* `guides/` a condensed version of many performance/fixes from various places, e.g. the Arch Wiki, Stackoverflow, other forums and blogs
* `scripts/` contains some useful scripts mostly for non-daily use

All other files are dotfiles, which should be used only if the app is in use
