## Use obs!

## windows (doesn't work :()
```
ffmpeg -f dshow -s 1920x1080 -i video="gdigrab":audio="Microphone (Realtek High Definition Audio)" out.mkv
```

## linux 
X11:
```
ffmpeg -loglevel debug -threads:v 2 -threads:a 8 -filter_threads 2 \
-thread_queue_size 512 -f x11grab -s 1920x1080 -framerate 60 -i :0.0 \
-thread_queue_size 512 -f pulse -i default -acodec ac3 \
-c:v h264_nvenc -preset slow  -b:v 8M -bufsize 16M -profile:v high \
-bf 3 -b_ref_mode 2 -temporal-aq 1 -rc-lookahead 20 -vsync 0 out.mkv
```

Alternative audio codecs: libopus, flac

## higher framerates are only possible via https://trac.ffmpeg.org/wiki/Capture/Desktop#HardwareEncoding
## https://gist.github.com/Brainiarc7/4636a162ef7dc2e8c9c4c1d4ae887c0e

## also you can add -i music.flac
