1. Download the latest root (корневой) certs from `https://pki.gov.kz/` and get the base of NCLayer `https://pki.gov.kz/ncalayer/`
2. Put the certs into the `certs` folder, NCLayer should be nearby
3. Run `sudo ./run.sh`
