#!/bin/bash

# Should work on Arch, Debian, Ubuntu-like

# Root check
if [[ "$EUID" -ne 0 ]]; then
	echo "Error: run as root"
	exit
fi

# Copy root certificates, enable them, then run NCLayer, and delete the certs anyways
cp ./certs/{nca_gost.crt,nca_rsa.crt,root_gost.crt,root_rsa.crt} /etc/ca-certificates/trust-source/anchors/ && update-ca-trust && sh ./ncalayer.sh; rm /etc/ca-certificates/trust-source/anchors/{nca_gost.crt,nca_rsa.crt,root_gost.crt,root_rsa.crt} && update-ca-trust
