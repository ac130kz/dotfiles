# https://securitronlinux.com/bejiitaswrath/set-a-very-nice-linux-shell-prompt-and-very-useful-aliases/
# colors
red = r"\[\033[0;31m\]"
white = r"\[\033[38;5;15m\]"
cyan = r"\[\033[38;5;6m\]"
yellow = r"\[\033[38;5;11m\]"
green = r"\[\033[0;32m\]"
purple = r"\[\033[0;35m\]"

# tokens
user = r"\u"
host = r"\h"
directory = r"\w"

ps = red + "┌─[" + green + user + yellow + "@" + purple + host + red + \
    r"]:[" + cyan + directory + red + "]" + r"\n" + white + r"\$ "
print(ps)
