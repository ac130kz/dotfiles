#!/bin/sh
# DEPRECATED: use /etc/nftables.conf
# Simple firewall rules to satisfy common desktop needs
if test "$(id -u)" -ne "0"
then
    echo "ERROR: not root" >&2
    exit 1
fi

for table in iptables # FIXME: ip6tables doesn't work
do
    echo "[$table]: Flushing existing rules"
    $table -F

    echo "[$table]: Setting up default policies"
    $table -P INPUT DROP
    $table -P FORWARD DROP
    $table -P OUTPUT ACCEPT

    echo "[$table]: Dropping invalid packets"
    $table -A INPUT -m conntrack --ctstate INVALID -j DROP

    echo "[$table]: Allowing established and related connections"
    $table -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
    $table -A OUTPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

    echo "[$table]: Allowing all loopback traffic"
    $table -A INPUT -i lo -j ACCEPT
    $table -A OUTPUT -o lo -j ACCEPT

    # icmpv4 is no longer required by the RFC for proper tcp congestion
    # FIXME: doesn't handle icmpv6, which actually requires
    #echo "[$table]: Rejecting INPUT icmp per RFC, accepting OUTPUT"
    #$table -A INPUT -p icmp -j REJECT
    #$table -A OUTPUT -p icmp -j ACCEPT

    echo "[$table]: Saving new rules, adding persistence"
    $table-save > /etc/iptables/$table.rules
    systemctl enable $table.service
done

