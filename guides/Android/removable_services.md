# Patterns for "1-Click ops" blocking via AppManager

```
com.google.android.gms.measurement
com.google.android.gms.tagmanager
com.google.android.gms.analytics
com.google.android.gms.ads
com.google.ads
com.google.firebase.analytics
com.google.firebase.firebase_analytics
com.yandex.mobile.ads
com.yandex.metrica
com.qq.e.ads
com.baidu.location
com.facebook.ads
com.facebook.marketing
com.adjust
com.mapbox.android.telemetry
```
