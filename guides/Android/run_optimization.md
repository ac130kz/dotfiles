The default is `speed-profile`, which only does some profile based call tracing and doesn't apply space optimizations.
Generally you want to use `speed` if there are no profiles to use (newly installed ROM) or if you 
want to optimize to the max (it takes time...). Otherwise, stick to PGO based `speed-profile`
recorded by Android. There are also `everything-profile` and `everything`, which add space optimizations.
Finally, on Android < 14 the same command should be run with `--compile-layouts` separately to precompile layouts as well.

```bash
# doesn't actually require full root, just common uid rights provided by the adb
# -a means all
# -f means force
# --full includes extra dexes and their dependencies, unlike the default mode
pm compile -a -f --full -m "speed"
```

Doing it in the way the OS does ("speed-profile"):
```bash
adb shell cmd package bg-dexopt-job
```

To reset all the profiles, do this:
```bash
pm compile --reset -a
```
