## Backup via TWRP adb
```bash
adb backup --twrp
```
* to restore: 
```bash
adb restore <filename>
```

## Get packages adb
```bash
adb shell 'pm list packages -f' | sed -e 's/.*=//' | sed 's/\r//g' | sort > packages.txt
```

## Sysctl
Use Sysctl GUI app to set:
```
net.ipv4.tcp_congestion_control=bbr
net.ipv4.tcp_notsent_lowat=16384
net.ipv4.ip_default_ttl=65
vm.swappiness=7
net.ipv4.tcp_fastopen=3
vm.min_free_kbytes=131070
net.ipv4.tcp_fin_timeout=7
```

## Flash recovery without USB2.0 via adb shell
```bash
adb shell
su
dd if=/sdcard/twrp.img of=/dev/block/bootdevice/by-name/recovery
```
