1. Download a ROM without Gapps, should support signature spoofing (or patch Android `https://shadow53.com/android/no-gapps/setup-guide/1/#step-12-microg-signature-spoofing-support`)
2. Install Magisk
3. Download and install the latest .zip of microG Core or use Magisk
4. Once installed allow all the permissions in microG settings
5. For the store use Aurora Store (Google Play), Foxy Droid (F-Droid)
6. Sign in into all your accounts in microG settings
7. Install DAVx5, then setup your account as here: `https://www.davx5.com/tested-with/google`. Requires 2FA authentication, then app password in Google Account settings. Leaving a copy of base url, just in case: `https://www.google.com/calendar/dav/xxx@example.com/events`. Reboot afterwise. Unfortunately, it's a bit buggy, so I'd rather store CalDav and Contacts myself.
