## How to get the list of currently installed explicit packages without dependencies?

```bash
pacman -Qe
```

## How to list orphans?

```bash
pacman -Qtdq
```

## Deleting orphans

EXTREME WARNING: be careful, these will add more packages than listed above (i.e. needed optional), so it's preferable to check with the one above and delete only some truly unnessesary ones

```bash
pacman -Rns $(pacman -Qtdq)
```

## Android

```
android-aarch64-qt5 android-apktool android-udev jadx-gui-desktop
```

Development packages (NOTICE: use manually downloaded versions, which you put into /opt instead):
```
android-ndk android-platform android-sdk android-sdk-build-tools android-sdk-platform-tools android-studio flutter
```

## Arduino

```
arduino arduino-avr-core arduino-mk-git
```

## Audio
Pipewire on Wayland with Wlroots:
```
alsa-tools pulsemixer pipewire-alsa pipewire-pulse wireplumber xdg-desktop-portal xdg-desktop-portal-wlr deadbeef portaudio
```

Editing:
```
reaper-bin
```

## Base

For the improved kernel use:
```
https://gitlab.com/ac130kz/linux-custom
```

```
asp devtools linux linux-firmware grub grub-customizer gtk3 intel-ucode intel-undervolt iw iwd networkmanager openresolv os-prober pacman sudo paru nftables iptables-nft dbus-broker dbus-broker-units x86_energy_perf_policy thermald
```

Wayland base:
```
fuzzel i3status glfw-wayland libglvnd lib32-libglvnd vulkan-intel intel-media-driver xorg-xwayland xorgproto libdrm drm_info grim nnn sway swaybg swayidle swaylock wlroots eglexternalplatform mako gst-plugin-pipewire pipewire-v4l2 slurp wlrobs xdg-desktop-portal-wlr wf-recorder digikam gst-plugins-bad gst-libav gst-plugins-good gst-plugins-ugly qt6-wayland qt6ct xdg-desktop-portal-wlr wlrobs wf-recorder wl-clipboard skim gtk4 xorg-xhost mesa intel-media-sdk brightnessctl
```

## Bluetooth

```
bluez bluez-utils
```

## Browser

```
chromium firefox geckodriver
```

## Dev common and some deps

```
base-devel bc kmod libelf pahole xmlto python-sphinx python-sphinx_rtd_theme graphviz imagemagick docker containerd flex gradle graphviz groff jdk-openjdk jdk libfdk-aac libheif libsamplerate libtool llvm m4 make mkcert mtools nodejs npm pacman-contrib patch pkgconf postgresql sdl visual-studio-code-bin
```

OSS VS Code (worse for Python):

```
vscodium-bin
```

Set default Java to whatever is the latest Oracle JDK `java-16-jdk`:

```bash
sudo archlinux-java set java-16-jdk
```

## Dev C/C++

```
boost ccache cmake conan gcc gdb intel-mkl-static openmp openmpi valgrind
```

## Rust + required binaries

Install using the common shell script with a full nightly profile and `mold` linker.

Some useful binaries:
```
cargo install broot protobuf-codegen skim diesel_cli flamegraph hyperfine kickoff nu swayr prql-compiler cargo-nextest
```

## Dev Python + ML + DL

NOTICE: filter out the mess and prefer to use virtualenv where possible (except maybe ultra-optimized/system-wide required packages):
```
jupyter-notebook pyside2 pyside2-tools python python-aiohttp python-beautifulsoup4 python-babel python-brotli python-cssselect python-h2 python-imread python-jedi python-language-server python-matplotlib python-more-itertools python-pandas python-pip python-pylint python-pynvim python-pyqt5 python-pytest python-pytorch-opt-cuda magma-cuda python-rope python-scikit-image python-scikit-learn python-scipy python-selenium python-setuptools python-setuptools-git python-simpleitk python-tabulate python-tensorflow-cuda python-xlrd python-xlwt tensorboard tensorflow-cudatk rust-analyzer python-gym python-snakeviz
```

## Julia

```
julia
```

```
julia>] add MKL
```

## Qt + Dev

```
qcustomplot qt5-base qt5-connectivity qt5-examples qt5-quick3d qt5-serialbus qt6ct qt6-base qtcreator qt6-sensors qt6-connectivity qt6-serialport qt6-websockets qt6-charts qt6-webengine qt6-networkauth python-pyqt6 pyside6 qt6-multimedia qt6-multimedia-ffmpeg
```

## Fonts

```
noto-fonts ttf-anonymous-pro ttf-fira-code ttf-font-awesome ttf-joypixels ttf-lato ttf-liberation ttf-opensans ttf-roboto ttf-ubuntu-font-family ttf-jetbrains-mono ttf-cascadia-code otf-monaspace
```

## Guitar

```
ardour tuxguitar guitarix
```

## Latex

```
biber texinfo texlive-bibtexextra texlive-core texlive-fontsextra texlive-formatsextra texlive-games texlive-humanities texlive-latexextra texlive-music texlive-pictures texlive-pstricks texlive-publishers texlive-science texstudio typst
```

## Nvidia (use `dkms` if using any custom kernel):
Standard method:
```
nvidia-open-dkms nvidia-settings nvidia-utils lib32-nvidia-utils opencl-nvidia lib32-opencl-nvidia libglvnd lib32-libglvnd
```

Manual method (own Frogging-Family/nvidia-all fork): `makepkg -si` + extra packages `ocl-icd`

## Tools

```
aria2 yt-dlp bat bash-completion bpf chntpw dosfstools ect edid-decode-git efibootmgr fakeroot file freeimage findutils gawk gettext git git-lfs gnome-keyring grep glances gzip jpegoptim ksshaskpass lshw mesa-utils net-tools parallel p7zip pigz rclone sed tree unrar wavpack wget which wireguard-tools wireshark-cli ripgrep fd gdu
```

## Progs

```
evince baobab masterpdfeditor-free file-roller geany gimp hddtemp htop inkscape keepassxc lxappearance neofetch neovim-git mpv obs-studio quiterss pcmanfm smartmontools telegram-desktop torbrowser-launcher vlc wireshark-qt xterm popcorntime-bin zoom onlyoffice-bin opendoas rclone shotcut
```

Git LFS setup:

```bash
git lfs install
git lfs track "*.csv"
```

## Printing
```
cups cups-browsed
```

HP:
```
hplip
```

Start the cups service, then use the built-in http://localhost:631 UI server.

## Virtualbox (run `modprobe vboxdrv` before running the app)

```
virtualbox virtualbox-host-dkms
```

## ROS (noetic, Ubuntu 20.04)

```
ros-noetic-desktop-full ros-noetic-ros-controllers
```

## QEMU

```
qemu-desktop virt-manager libvirt edk2-ovmf ebtables dnsmasq
```

Don't forget to add your user to the libvirt group: `usermod -aG libvirt user`

To start: `systemctl start libvirtd && virsh net-autostart default`, then `virt-manager`

For a second GPU passthrough support:

- Add to GRUB kernel parameters `intel_iommu=on iommu=pt`
- Add to MODULES in mkinitcpio `vfio_pci vfio vfio_iommu_type1 vfio_virqfd`

## Mining

* Monero: `xmrig-donateless xmrig-cuda monero-gui`

