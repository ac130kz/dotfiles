## To customize font and theme
```
lxappearance
```

Font config:
JetBrains Mono 7, AA, full (or maybe slight) hinting, sub-pixel is probably RGB (see display.md)

Widget theme (requires some packages, search on Arch Wiki):
Adwaita-dark

Icons (requires some packages, search on Arch Wiki):
Breeze Dark

Cursor:
Adwaita

## Installing Windows fonts
```bash
sudo mkdir /usr/share/fonts/WindowsFonts
sudo cp /mnt/windows/Windows/Fonts/* /usr/share/fonts/WindowsFonts/
sudo chmod 644 /usr/share/fonts/WindowsFonts/*
fc-cache --force
```
