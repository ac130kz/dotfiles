## Scarlett Solo setup

Create a modprobe.d config as usual to get a mixer driver working:

```
options snd_usb_audio vid=0x1235 pid=0x8211 device_setup=1
```

## [OPTIONAL] Disable powersaving features

Create a modprobe.d config as usual:

```
options snd_hda_intel power_save=0
```
