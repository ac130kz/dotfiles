## Remove annoying stuff in google

Via Ublock Origin, paste the following in the custom blocks (`https://acuelight.wordpress.com/2017/01/14/using-xpath-filters-with-ublock-origin`):

## Extensions

```
Base:
Ublock Origin (all filters, except non-russian)
Sponsorblock
Vimium/Vimium-C

Optional:
Violentmonkey
```

## AV1

```
# only enable if supported hardware
media.av1.enabled true
```

## Rethink DNS

DoH
```
http://1-6cpqaoh7ap7777776ko757477h7ohwn7laua.max.rethinkdns.com
```

DoT
```
1-6cpqaoh7ap7777776ko757477h7ohwn7laua.max.rethinkdns.com
```

## about:config

It's possible to do this using `user.js` like this:

```javascript
user_pref("browser.display.show_image_placeholders", false);
```

Most of these are from: https://habr.com/ru/post/459880/

## YT

Install Enchancer for Youtube, here's the config:

```
{"version":"2.0.112","settings":{"blur":0,"brightness":100,"contrast":100,"grayscale":0,"huerotate":0,"invert":0,"saturate":100,"sepia":0,"applyvideofilters":false,"backgroundcolor":"#000000","backgroundopacity":50,"blackbars":false,"blockads":false,"blockadsexceptforsubs":false,"blockautoplay":false,"blockhfrformats":false,"blockwebmformats":false,"cinemamode":false,"cinemamodewideplayer":true,"controlbar":{"active":false,"autohide":false,"centered":true,"position":"absolute"},"controls":[],"controlsvisible":false,"controlspeed":true,"controlspeedmousebutton":false,"controlvolume":true,"controlvolumemousebutton":true,"customcolors":{"--main-color":"#00adee","--main-background":"#111111","--second-background":"#181818","--hover-background":"#232323","--main-text":"#eff0f1","--dimmer-text":"#cccccc","--shadow":"#000000"},"customcssrules":"","customscript":"","customtheme":false,"darktheme":false,"date":0,"defaultvolume":true,"disableautoplay":true,"executescript":false,"expanddescription":true,"filter":"none","hidecardsendscreens":true,"hidechat":false,"hidecomments":false,"hiderelated":false,"ignoreplaylists":true,"ignorepopupplayer":true,"localecode":"en_US","localedir":"ltr","message":false,"miniplayer":false,"miniplayerposition":"_top-right","miniplayersize":"_640x360","newestcomments":false,"overridespeeds":false,"pauseforegroundtab":false,"pausevideos":false,"popuplayersize":"640x360","qualityembeds":"hd720","qualityembedsfullscreen":"hd1080","qualityplaylists":"hd1080","qualityplaylistsfullscreen":"hd1080","qualityvideos":"hd1080","qualityvideosfullscreen":"hd1080","reload":false,"reversemousewheeldirection":false,"selectquality":true,"selectqualityfullscreenoff":false,"selectqualityfullscreenon":false,"speed":1,"speedvariation":0.1,"stopvideos":false,"theatermode":false,"theme":"default-dark","themevariant":"youtube-deep-dark.css","update":1617814753791,"volume":7,"volumemultiplier":3,"volumevariation":1,"whitelist":"","wideplayer":false,"wideplayerviewport":false}}
```

