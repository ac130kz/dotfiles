## Known garbage services (for desktop use)
https://askubuntu.com/questions/999662/boot-time-ubuntu-16-04

https://www.linux.com/learn/cleaning-your-linux-startup-process

```
sudo systemctl disable <service name>
sudo systemctl stop <service name>

smbd.service 
lvm2-monitor.service
nmbd.service
snapd.service 
snapd.seeded.service
apt-daily.timer
apt-daily-upgrade.timer
pppd-dns.service
avahi-daemon.service
motd-news.service
NetworkManager-wait-online.service
```

If it's still there, then mask it:
```
sudo systemctl mask apt-daily.service apt-daily-upgrade.service
```

Useless emergency, used only on the servers:
```
sudo systemctl mask emergency.service
sudo systemctl mask emergency.target
```

## Debugging the startup
```
ac130kz@pc:~$ systemd-analyze plot > filename.svg
ac130kz@pc:~$ systemd-analyze
Startup finished in 5.548s (firmware) + 2.033s (loader) + 5.154s (kernel) + 55.548s (userspace) = 1min 8.286s
graphical.target reached after 44.940s in userspace
ac130kz@pc:~$ systemd-analyze blame
10.669s systemd-journal-flush.service                                            
 9.431s ldconfig.service                                                         
 4.704s systemd-udevd.service                                                    
 3.979s initrd-switch-root.service                                               
 2.247s systemd-logind.service                                                   
 1.926s NetworkManager.service                                                   
 1.211s cpupower.service                                                         
 1.200s dhcpcd.service                                                           
 1.056s systemd-sysusers.service                                                 
  994ms polkit.service
 ....
```

## Useless splash on boot
Remove splash from grub config via grub-customizer

## Reducing the journal storage amount - less lags during the startup
```
sudo journalctl --vacuum-time=2d
```
