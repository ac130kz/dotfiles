Starting up the server (`tigervnc` package):
```bash
vncpasswd
x0vncserver -rfbauth ~/.vnc/passwd
```

Connect via TightVNC (Windows) or TigerVNC