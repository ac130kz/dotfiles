## TODO: bad news
Video decoding seems to work just on the software, the required flag breaks the browser:
```
--use-gl=desktop
```


## On Arch

Currently, it seems that hw decoding has been merged into the base package, so one
can install a usual `chromium`

On Nvidia we have to install an additional package: `libva-vdpau-driver-chromium`

Just paste the configuration file line by line `.config/chromium-flags.conf`

## NOTE: `--enable-vulkan` is broken - video glitches and hangs

## NOTE: source of truth for all the flags (example search): `https://source.chromium.org/chromium/chromium/src/+/master:chrome/browser/about_flags.cc;l=5819?q=enable-heavy-ad-intervention&sq=&ss=chromium`
```
--audio-worklet-realtime-thread
--cipher-suite-blacklist=0xc013,0xc014,0x009c,0x009d,0x002f,0x0035,0x000a
--compositor-threaded-scrollbar-scrolling
--disable-breakpad
--disable-logging
--disable-smooth-scrolling
--disable-speech-synthesis-api
--dns-over-https
--dynamic-tcmalloc-tuning
--enable-accelerated-video-decode
--enable-defer-all-script
--enable-defer-all-script-without-optimization-hints
--enable-gpu-rasterization
--enable-heavy-ad-intervention
--enable-javascript-harmony
--enable-lazy-frame-loading
--enable-lazy-image-loading
--enable-new-download-backend
--enable-noscript-previews
--enable-parallel-downloading
--enable-preconnect-to-search
--enable-quic
--enable-subresource-redirect
--enable-tls13-early-data
--enable-webgl-image-chromium
--enable-zero-copy
--heavy-ad-privacy-mitigations
--incognito
--legacy-tls-enforced
--new-canvas-2d-api
--no-crash-upload
--no-default-browser-check
--no-pings
--no-report-upload
--num-raster-threads=6
--reduced-referrer-granularity
--show-legacy-tls-warnings
--ssl-version-min=tls1.2
--texture-layer-skip-wait-for-activation
--ui-enable-zero-copy
--v8-cache-options=code
--disable-font-subpixel-positioning
--enable-features=WebUIDarkMode
--enable-oop-rasterization
--enable-oop-rasterization-ddl
--ignore-gpu-blocklist
--ignore-gpu-blacklist
--enable-vulkan
--enable-skia-renderer
--ui-enable-rgba-4444-textures
--enable-rgba-4444-textures
--enable-native-gpu-memory-buffers
--decode-jpeg-images-to-yuv
--decode-webp-images-to-yuv
--enable-features=VaapiVideoDecoder
--enable-webrtc-pipewire-capturer
--enable-features=UseOzonePlatform
--ozone-platform=wayland
--enable-raw-draw
```

## Questionable flags
`https://bugs.chromium.org/p/chromium/issues/detail?id=824153`
```
--disable-font-subpixel-positioning
```

## On Ubuntu
`/etc/chromium-browser/customizations`

add into the example
`sudo nano 00-example`

Warning (just an example, check Arch flags above):
```
CHROMIUM_FLAGS="${CHROMIUM_FLAGS} --incognito"
```

## Maybe also follow extra steps for Ublock Origin here
https://jspenguin2017.github.io/uBlockProtector/
