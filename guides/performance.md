### Undervolting

The included config is great on high quality thermal paste, like PTM7950.

Put this to `/etc/intel-undervolt.conf`. Some measurements on bad thermal paste: default 107/70 (ez 94 degrees at which throttling starts), 90/50 boosted (3.65 peaks, small throttling, up to 90 degrees), 65/45 my own balanced (super stable 3.5GHz, 85ish degrees), 45/40 recommended by others:

```
enable yes
undervolt 0 'CPU' -120.00
undervolt 1 'GPU' -0.00
undervolt 2 'CPU Cache' -120.00
power package 85 70
```

There's also this incompatibility, which doesn't allow to write into MSR registers to be fixed with GRUB_CMDLINE_LINUX_DEFAULT:

```
msr.allow_writes=on
```

Additional information:
https://github.com/kitsunyan/intel-undervolt
https://wiki.archlinux.org/index.php/Improving_performance

## Stable CPU temp path

`/etc/udev/rules.d/50-stable-cpu-temp-path.rules`

```
# Give CPU temp a stable device path
# https://joonas.fi/2021/07/stable-device-path-for-linux-hwmon-interfaces
# https://wiki.archlinux.org/title/lm_sensors#Persistent_device_names

# Intel coretemp
ACTION=="add", SUBSYSTEM=="hwmon", ATTRS{name}=="coretemp", RUN+="/bin/sh -c 'ln -s /sys$devpath/temp1_input /dev/cpu_temp'"
```

## Set a bit more aggressive CPU performance 

First, add x86_energy_perf_policy to sudoers (`/etc/sudoers.d/x86_energy_perf_policy`):
```
ALL ALL=(root) NOPASSWD: /usr/bin/x86_energy_perf_policy
```

`/etc/udev/rules.d/71-cpu.rules`
```
KERNEL=="cpu", SUBSYSTEM=="event_source", ACTION=="add", RUN+="/bin/sh -c 'x86_energy_perf_policy --hwp-enable --hwp-max 45 --hwp-min 30 --epb 4 --hwp-epp 128 --turbo-enable 1'"
```

### Setting up preferred ioschedulers for the disk type

`/etc/udev/rules.d/60-ioschedulers.rules`

```
# set scheduler for NVMe
ACTION=="add|change", KERNEL=="nvme[0-9]*", ATTR{queue/scheduler}="none"
# set scheduler for SSD and eMMC
ACTION=="add|change", KERNEL=="sd[a-z]|mmcblk[0-9]*", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="mq-deadline"
# set scheduler for rotating disks
ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"
```

### Disabling mitigations (x86)

As simple as setting `mitigations=off` to the kernel startup parameters.

Or with some granular parameters from `https://make-linux-fast-again.com` and other sources:

```
noibrs noibpb nopti nospectre_v2 nospectre_v1 l1tf=off nospec_store_bypass_disable no_stf_barrier mds=off tsx=on tsx_async_abort=off gather_data_sampling=off kvm.nx_huge_pages=off l1tf=off mmio_stale_data=off retbleed=off spec_store_bypass_disable=off spectre_v2_user=off srbds=off
```

### x86_64 v3
Modify `/etc/pacman.conf` by putting these on top of the system's default mirrors:

CachyOS repos (`https://wiki.cachyos.org/en/home/Repo`):
```
## cachyos repos
[cachyos-v3]
Include = /etc/pacman.d/cachyos-v3-mirrorlist
[cachyos-core-v3]
Include = /etc/pacman.d/cachyos-v3-mirrorlist
[cachyos-extra-v3]
Include = /etc/pacman.d/cachyos-v3-mirrorlist
[cachyos]
Include = /etc/pacman.d/cachyos-mirrorlist
```

### Use dbus-broker
It is faster than default dbus:
```bash
sudo systemctl enable dbus-broker.service
sudo systemctl --global enable dbus-broker.service
```

### Turbo boost missing after a while fixed
```bash
sudo systemctl enable reactivate-boost.timer && sudo systemctl start reactivate-boost.timer
```

### Disabling ipv6

To be on disabling extras from ipv6:

```
ipv6.disable=1
```

### Defragging ext4 on HDD without unmounting

Useless on SSDs, yet very important on older systems with HDDs:

```bash
sudo e4defrag /
```

### (ONLY FOR SERVERS) Reliable clocksource for databases like Clickhouse
`tsc=reliable clocksource=tsc`

### (OPTIONAL) Disable core dumps

To improve performance and save disk space. This alone disables the saving of coredumps but they are still in memory.
Edit `/etc/systemd/coredump.conf`, under `[Coredump]` uncomment `Storage=external` and replace it with `Storage=none`. Then:

```bash
sudo systemctl daemon-reload
```

If you want to disable core dumps completely add to `/etc/security/limits.conf`:

```
* hard core 0
```

### (OPTIONAL) Use dptf to control power (it's use is debatable)

```bash
paru -S dptf && sudo systemctl enable dptf && sudo systemctl start dptf
```

### (OPTIONAL) Disable watchdog

Blacklist `iTCO_wdt` module as usual:

```
blacklist iTCO_wdt
install iTCO_wdt /usr/bin/false
```
