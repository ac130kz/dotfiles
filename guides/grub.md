## Regenerating after changes to defaults in `/etc/default/grub`
```bash
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

## Restoring GRUB
1. Get Arch ISO
2. Boot off it
3. Look up what disks are used: `fdisk -l`
4. Proceed
```bash
# mount root
mkdir /mnt/arch
mount -t auto /dev/sda2 /mnt/arch
# chroot
arch-chroot /mnt/arch
# mount my EFI folder
mount -t auto /dev/sda1 /efi
# run os-prober
os-prober
# copy new grub config
grub-mkconfig -o /boot/grub/grub.cfg
# install grub
grub-install --efi-directory=/efi --target=x86_64-efi /dev/sda
```

Credits: `https://www.jeremymorgan.com/tutorials/linux/how-to-reinstall-boot-loader-arch-linux/`
