## General setup
`/etc/mkinitcpio.conf`:
```
MODULES=(i915 nvidia nvidia_uvm nvidia_drm nvidia_modeset)
## nvidia-settitngs are shown below in DRM
FILES=("/etc/modprobe.d/blacklist.conf" "/etc/modprobe.d/nvidia-settings.conf")

HOOKS=(keyboard systemd autodetect modconf block filesystems fsck shutdown)
COMPRESSION="lz4"
COMPRESSION_OPTIONS="-9"
```

## Blacklisting shitty modules
`/etc/modprobe.d/blacklist.conf`
```
blacklist nouveau
blacklist rivafb
blacklist nvidiafb
blacklist rivatv
blacklist nv
blacklist uvcvideo
install nouveau /bin/false
```

## i915 driver options
`/etc/modprobe.d/i915.conf`
Guc enables the firmware for proper performance (2 - gen 9 and newer, 3 - Alder Lake), psr is powersaving.

```
options i915 enable_guc=2 enable_psr=0 enable_fbc=1
```

## DRM loading is required to enable vsync (on ubuntu after that run: `sudo update-initramfs -u`)
`/etc/modprobe.d/nvidia-drm.conf`
https://wiki.gentoo.org/wiki/NVIDIA/nvidia-drivers/

```
options nvidia NVreg_UsePageAttributeTable=1 NVreg_InitializeSystemMemoryAllocations=0 NVreg_EnableStreamMemOPs=1 NVreg_EnableMSI=1 NVreg_EnablePCIeGen3=1 NVreg_EnablePCIERelaxedOrderingMode=1 NVreg_NvLinkDisable=1 NVreg_PreserveVideoMemoryAllocations=1
```

## Disable Nvidia useless devices

Especially the HDA, to get rid of sd-umon errors https://bugs.archlinux.org/task/63697

`/etc/udev/rules.d/70-disable-nvidia-devices.rules`:
```
# Remove NVIDIA USB xHCI Host Controller devices, if present
ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x0c0330", ATTR{remove}="1"

# Remove NVIDIA USB Type-C UCSI devices, if present
ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x0c8000", ATTR{remove}="1"

# Remove NVIDIA Audio devices, if present
ACTION=="add", SUBSYSTEM=="pci", ATTR{vendor}=="0x10de", ATTR{class}=="0x040300", ATTR{remove}="1"
```

## Enable services to prevent Nvidia CUDA from crashing
```bash
sudo systemctl enable nvidia-hibernate.service
sudo systemctl enable nvidia-resume.service
sudo systemctl enable nvidia-suspend.service
```

## Inside set
Dithering to 2x2 static, digital vibrance 50, color correction -> brightness -0.05

