Install the latest Arduino IDE with new compiler packages:
```
arduino arduino-avr-core
```

Add a udev rule: `/etc/udev/rules.d/01-ttyusb.rules`:
```
SUBSYSTEMS=="usb-serial", TAG+="uaccess"
```

In case of Java error set the latest Java by default:
```
$ archlinux-java status
Available Java environments:
java-12-openjdk
java-8-openjdk (default)

$ sudo archlinux-java set java-12-openjdk
```
