### How to fix broken uuid
* Use `blkid` to determine the UUID of your swap partition, and while at it, make sure all other partitions have correct UUID's in `/etc/fstab`. Also can use `lsblk -f` to find the UUID's.
* Put the correct UUID's into `/etc/fstab`, especially swap, for this error.
* Put the correct UUID for swap into `/etc/initramfs-tools/conf.d/resume`.
* Run `sudo update-initramfs -u`
* Reboot. 