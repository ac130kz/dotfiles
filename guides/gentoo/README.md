Installing gentoo
====================

### Note: LiveCD may not be bootable, then use old LiveDVD

## 1. Booting up

* press f1 and type 
        
        gentoo

- - -

## 2. Networking

* 2.1 Check networking:

        ping -c 3 www.google.com
        

* 2.2 In case of trouble with internet connection (For example, enp0s3):

        ip link set <network-interface> up
        
        
* Other type of configuring is:
        
        iwconfig <network-interface>
        
        net-setup <network-interface>
        
* 2.3 Now

        dhcpcd <network-interface>

- - -

## 3. Disk config

* 3.1 Using fdisk (For example, /dev/sda, disk may be DIFFERENT!):
        
        fdisk
        
        command d - deletes a partition
        
        command p - lists all partitions
        
        command n - creates new (p - primary)

        command w - write and exit
        
        command q - exit without changes
        
* 3.2 Now format and create swap (for example, /dev/sda4 and /dev/sda3), mount the root
        
        mkfs.ext4 <__new_root_path__>
        
        mkswap <__new_swap_path__>
        
        mount <__new_root_path__> /mnt/gentoo

- - -

## 5. Time

* 5.1 To check
        
        date

* 5.2 To make automatic
        
        ntpd -q -g

- - -

## 6. Stage 3

* 6.1 Download and unpack
        
        cd /mnt/gentoo
        
        links https://www.gentoo.org/downloads/mirrors/
        
        * Choose Multilib for 64 bit + 32 bit or No-multilib for just 64 bit

        tar xvJpf stage3-*.tar.xz --xattrs --numeric-owner

* 6.2 /etc/portage/make.conf

        nano -w /mnt/gentoo/etc/portage/make.conf
        
* 6.3 Paste in this (Maybe -ftree-vectorize should be used to utilize YMM registers, but not recommended - causes segfaults; setting -jX is changing the number of cores to use, may cause problems on low end machines, main is overheating and crashes)

        MAKEOPTS="-j2"
        
        CFLAGS="-O2 -s -pipe -march=native"
        
        CXXFLAGS="${CFLAGS}"
        
        CHOST="x86_64-pc-linux-gnu"

## 7. Installing

* 7.1 Select a mirror (Neolabs LPP)

        mirrorselect -i -o >> /mnt/gentoo/etc/portage/make.conf
        
* 7.2 Make a folder for repos and copy the config, save network config

        mkdir /mnt/gentoo/etc/portage/repos.conf
        
        cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf
        
        cp -L /etc/resolv.conf /mnt/gentoo/etc/

* 7.3 Mounting the shit up!

        mount -t proc /proc /mnt/gentoo/proc
        
        mount --rbind /sys /mnt/gentoo/sys
        
        mount --make-rslave /mnt/gentoo/sys
        
        mount --rbind /dev /mnt/gentoo/dev
        
        mount --make-rslave /mnt/gentoo/dev
        
* 7.4 Step into the system

        chroot /mnt/gentoo /bin/bash
        
        source /etc/profile
        
        export PS1="(chroot) $PS1"

Now we can leave! To "resume": repeat steps 7.3, 7.4

* 7.5 Syncing, updating ebuilds

        emerge-webrsync
        
        emerge --sync
        
* 7.6 Optional: List, read, delete news
       
        eselect news list
       
        eselect news read

        eselect news purge

* 7.7 Choosing the right profile (gnome/kde/xfce/ and ...)

        eselect profile list
        
        eselect profile set <profile-name>
        
* 7.8 Just a safety stuff

        emerge --ask --update --deep --newuse @world

* 7.9 Configuring the USE variable
        
WILL show current USE
        
        emerge --info | grep ^USE
        
        nano -w /etc/portage/make.conf
        
ADD (use default flags, then remove extras, like amdgpu)

        USE="gtk gnome python pulseaudio qt5 alsa mmx sse sse2 unicode"
        
        VIDEO_CARDS = " "
        
        INPUT_DEVICES = " "
        
        ACCEPT_LICENSE = "* @EULA"
        

* 7.10 Setting timezone
        
        echo "Asia/Almaty" > /etc/timezone
        
        emerge --config sys-libs/timezone-data
        
* 7.11 Setting up the locale

        nano /etc/locale.gen
        
UNCOMMENT
        
        en_US.UTF-8 UTF-8
        
ADD

        locale-gen
        
        eselect locale list
        
        eselect locale set 3
        
        env-update && source /etc/profile && export PS1="(chroot) $PS1"
        
## 8. Building kernel

        emerge --ask sys-kernel/gentoo-sources
        
        emerge -av freefonts corefonts cronyx-fonts terminus-font udev syslog-ng dhcpcd vixie-cron pciutils usbutils
        
        ls -l /usr/src/linux
        
## 8.1 Stuff

You can now apply optimization patch (or others) `https://github.com/graysky2/kernel_gcc_patch` to /usr/src/linux by using:
```bash
patch -p1 < ...patch
```
        
## 8.2 Resolve the conflicts like (USE="firmware cryptsetup" emerge --ask sys-kernel/genkernel)
        
        emerge --ask sys-kernel/genkernel
        
        genkernel --menuconfig all
        
## 8.3 Menuconfig

Now remove extras like amdgpu, amd cpu support, inifinitiband, 
set optimization to native, disable ipv6, set reno as default tcp congestion,
increase audio buffer to 2048, xz or lzma compression, ORC unwinder, 300HZ timer.
Disable SE Linux, disable strong stackprotector.
Set `CONFIG_DEBUG_INFO=n`. `make localmodconfig` chooses only what is installed.

## 8.4 Finalizing

        ls /boot/kernel* /boot/initramfs*

        emerge --ask sys-kernel/linux-firmware

## OPTIONAL: udev can do it automatically, but we can help - list all the modules that are relevant in the file

```bash
find /lib/modules/<kernel version>/ -type f -iname '*.o' -or -iname '*.ko' | less
mkdir -p /etc/modules-load.d
nano -w /etc/modules-load.d/network.conf 
```

## 8.5 Setup fstab (list UUIDs by `blkid`)

        nano -w /etc/fstab

Simplest one
```
/dev/sda5   none         swap    sw                   0 0
/dev/sda6   /            ext4    noatime              0 1
```

## 8.6 Setup hostname and other stuff

        nano -w /etc/conf.d/hostname
        
        emerge --ask --noreplace net-misc/netifrc
        
        nano -w /etc/conf.d/net

Paste: config_eth0="dhcp"

## 8.7 Networking (subsitute eth0 by your available interface given by `ifconfig`)

        cd /etc/init.d
        
        ln -s net.lo net.eth0
        
        rc-update add net.eth0 default
 
## OPTIONAL: review configs (OpenRC, keyboard layout, system clock)

        nano -w /etc/rc.conf
        
        nano -w /etc/conf.d/keymaps
        
        nano -w /etc/conf.d/hwclock

## 8.8 Additional software

        emerge --ask sys-fs/e2fsprogs
        
        emerge --ask net-misc/dhcpcd
        
        #emerge --ask net-wireless/iw net-wireless/wpa_supplicant

## 9. Setup a user

* 9.1 Add a password for root
        
        passwd

* 9.2 Add a new user

        useradd -m -G users,audio,usb,video,wheel ac130kz
        
        passwd ac130kz
        
* 9.3 Switch between user and root

        su - ac130kz
        
* 9.4 Install sudo (uncomment wheel all)

        emerge --ask app-admin/sudo
        
        su -
        
        nano /etc/sudoers
        
## 10. Setting up the environment

        emerge --ask dev-libs/sway
