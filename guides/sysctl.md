## How to edit:
`sudo nano /etc/sysctl.conf` or on Arch in `/etc/sysctl.d/99-<filename>.conf` (99 is required to make the last priority)

A ton of tweaks are taken from here: `https://klaver.it/linux/sysctl.conf` and `https://blog.cloudflare.com/optimizing-tcp-for-high-throughput-and-low-latency`.

Add the following, save and reboot (one can apply immediately with `sudo systemctl --system`):
```
# limit dmesg to sudo
kernel.dmesg_restrict=1

# more opened files possible
# (x*1024*1024*1024//1024)//10
# where x - amount of memory in gigabytes
# https://elixir.bootlin.com/linux/v6.10.9/source/fs/file_table.c#L525
# 10% of memory using 1k per handle
# but most recommend setting to infinity, e.g. 9223372036854775807
fs.file-max=9223372036854775807

# less swapping, writes optimized for bursts 
vm.swappiness=3
vm.dirty_ratio=60
vm.dirty_background_ratio=5

# less locking on dentries
vm.vfs_cache_pressure=50

# specifies the minimum virtual address that a process is allowed to mmap
vm.mmap_min_addr=65536

# 50% overcommitment of available memory, keep default heuristics
vm.overcommit_ratio=50
vm.overcommit_memory=0

# Keep at least 128MB of free RAM space available
vm.min_free_kbytes=131070

# setting up low ttl for less latency
net.ipv4.ip_default_ttl=65
# fq is a requirement for bbr congestion (fixed apparently https://github.com/systemd/systemd/issues/9087#issuecomment-394117038)
# some alternatives are: cake or popular default fq_codel (and its friend fq_pie)
net.core.default_qdisc=fq
# TCP congestion: reno/htcp are good for lossless, westwood is good for lossy only, bbr is versatile
# to show available: ls -la /lib/modules/$(uname -r)/kernel/net/ipv4 
# specifying a fallback
net.ipv4.tcp_congestion_control=htcp
net.ipv4.tcp_congestion_control=bbr
net.ipv4.tcp_notsent_lowat=131072

# some tcp tricks
# allows in and out fast open
net.ipv4.tcp_fastopen=3
# probing allows to maxout the throughput
net.ipv4.tcp_mtu_probing=1
# memory size tweaks
net.core.rmem_max=16777216
net.core.wmem_max=16777216
net.ipv4.tcp_wmem=8192 262144 536870912
net.ipv4.tcp_rmem=8192 262144 536870912
# this one can improve the overall startup time, but sometimes unable to start wifi
net.ipv4.tcp_slow_start_after_idle=0

# turn on the tcp_timestamps, accurate timestamp make TCP congestion control algorithms work better,
# but do not use for servers, because it is dangerous for some attacks
net.ipv4.tcp_timestamps=1

# Turn on the tcp_window_scaling
net.ipv4.tcp_window_scaling=1

# Enable ignoring broadcasts request
net.ipv4.icmp_echo_ignore_broadcasts=1

# Enable bad error message Protection
net.ipv4.icmp_ignore_bogus_error_responses=1

# don't cache ssthresh from previous connection
net.ipv4.tcp_no_metrics_save=1
net.ipv4.tcp_moderate_rcvbuf=1

# Enable Explicit Congestion Notification (RFC 3168), disable it if it doesn't work for you
net.ipv4.tcp_ecn=1
net.ipv4.tcp_reordering=3

# Prevent SYN attack, enable SYNcookies (they will kick-in when the max_syn_backlog reached)
net.ipv4.tcp_syncookies=1
net.ipv4.tcp_syn_retries=2
net.ipv4.tcp_max_syn_backlog=4096
net.ipv4.tcp_synack_retries=2

# Do not accept ICMP redirects (prevent MITM attacks)
net.ipv4.conf.all.accept_redirects=0
net.ipv4.conf.all.send_redirects=0
net.ipv4.conf.default.accept_redirects=0
net.ipv6.conf.all.accept_redirects=0
net.ipv4.conf.default.send_redirects=0
net.ipv6.conf.default.accept_redirects=0

# Decrease the time default value for tcp_fin_timeout connection
net.ipv4.tcp_fin_timeout=7

# time wait attack protection, maybe useless
net.ipv4.tcp_rfc1337=1

# udp too
net.ipv4.udp_rmem_min=16384
net.ipv4.udp_wmem_min=16384

# extra cloudflare patch specifics
net.ipv4.tcp_adv_win_scale=-2
net.ipv4.tcp_collapse_max_bytes=6291456
```

