### Firewall
Flushing old rules sometimes doesn't work, maybe do a reboot afterwards:

```bash
sudo systemctl enable nftables.service
```

### Disable powersaving stuff

```bash
modinfo iwlwifi | grep parm
```

`/etc/modprobe.d/iwlwifi.conf`, then add to /etc/mkinitcpio.conf and regenerate:

```
options iwlwifi power_save=0 swcrypto=0 led_mode=3 uapsd_disable=1 bt_coex_active=0
options iwlmvm power_scheme=1
```

Notice: due to the recent bugs 11n_disable=8 is very unstable, gives the best results.
https://www.reddit.com/r/archlinux/comments/k3eb06/poor_wifi_performance_on_arch_compared_to_ubuntu/

## Audio noise elimination, if nothing works

https://wireless.wiki.kernel.org/en/users/documentation/iw#setting_tx_power

To mitigate audio noise, set the txpower to lower than 1100 (mBm = dBm \* 100):

```
sudo iw phy#0 set txpower fixed 800
```

It's easier though to apply a pre-up script (`/etc/NetworkManager/dispatcher.d/pre-up.d/low-power`):

```bash
#!/bin/bash
iw phy#0 set txpower fixed 800
# kinda useless, but still
iw dev wlan0 set power_save off
```

Then:

```bash
sudo chmod a+x ./low-power
```

### Cool and new backend for wifi (yay -S iwd)

`/etc/NetworkManager/conf.d/wifi_backend.conf`

```
[device]
wifi.backend=iwd
```

MAC randomization
`/etc/iwd/main.conf`

```
[General]
AddressRandomization=network
```

## Enable custom global DNS by default

As usual put this to some config file in NetworkManager's directory: `/etc/NetworkManager/conf.d/global_dns.conf`

I've added home router as the first option.

```
[global-dns-domain-*]
servers=1.1.1.1,8.8.8.8,192.168.1.1
```

### Turn off hostname sending

NetworkManager by default sends the hostname to the DHCP server. Hostname sending can only be disabled per connection not globally (GNOME Bug 768076).

To disable sending your hostname to the DHCP server for a specific connection, add the following to your network connection file:

`/etc/NetworkManager/system-connections/your_connection_file`

```
...
[ipv4]
dhcp-send-hostname=false
...
[ipv6]
dhcp-send-hostname=false
```

### Pacman fast mirrors

```
curl -s "https://www.archlinux.org/mirrorlist/?country=KZ&country=RU&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' > /etc/pacman.d/mirrorlist
```

My current list is:

```
Server = https://mirror.ps.kz/archlinux/$repo/os/$arch
Server = https://mirror.hoster.kz/archlinux/$repo/os/$arch
Server = https://mirror.yandex.ru/archlinux/$repo/os/$arch
Server = https://mirror.rol.ru/archlinux/$repo/os/$arch
```

### Apt-fast

Configure it to use apt and 8 threads:

```
sudo add-apt-repository ppa:apt-fast/stable
sudo apt update
sudo apt install apt-fast -y
```

### Intel Wifi driver fix for debian based

- Search for firmware

```bash
dmesg
```

- add to /etc/apt/sources.list

```
# Debian 8 "Jessie"
deb http://httpredir.debian.org/debian/ jessie main contrib non-free
```

- terminal

```bash
sudo apt update && sudo apt install firmware-iwlwifi
modprobe -r iwlwifi ; modprobe iwlwifi
```

- list pci-e

```bash
lspci -nn
```
