https://jackaudio.org/faq/linux_rt_config.html
## Packages:
```
yay -S qjackctl ardour guitarix
```

## Adding a group to control io usage:
`/etc/security/limits.d/20-audio.conf`:
```
@audio   -  rtprio     95
@audio   -  memlock    unlimited
```

Then:
```
sudo groupadd audio
sudo usermod -a -G audio user
```