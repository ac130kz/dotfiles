### Pacman tricks, add these

```
ILoveCandy
Color
ParallelDownloads = 8

[multilib]
Include = /etc/pacman.d/mirrorlist
```

## Replacing sudo with doas

One can absolutely replace `sudo` with `doas`, no need to create wheels group or edit sudoers.

