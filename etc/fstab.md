Add `lazytime` (lazy time updates) and `relatime` (main lazy time updates) to the root partition on ext4. Add `discard` to
enable TRIM for SSDs on mount.
